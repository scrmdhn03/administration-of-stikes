﻿namespace S2RUniversity
{
    partial class CetakBuktiPembayaran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.txtNama = new MetroFramework.Controls.MetroTextBox();
            this.txtJurusan = new MetroFramework.Controls.MetroTextBox();
            this.txtPembayaran = new MetroFramework.Controls.MetroTextBox();
            this.txtTotalTerbilang = new MetroFramework.Controls.MetroTextBox();
            this.txtJumlah = new MetroFramework.Controls.MetroTextBox();
            this.btnPrint = new MetroFramework.Controls.MetroButton();
            this.btnMainMenu = new MetroFramework.Controls.MetroButton();
            this.txtTanggal = new MetroFramework.Controls.MetroTextBox();
            this.txtsemester = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(163, 93);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(45, 19);
            this.metroLabel1.TabIndex = 15;
            this.metroLabel1.Text = "Nama";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(163, 135);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(52, 19);
            this.metroLabel2.TabIndex = 16;
            this.metroLabel2.Text = "Jurusan";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(160, 252);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(131, 19);
            this.metroLabel3.TabIndex = 17;
            this.metroLabel3.Text = "Tanggal Pembayaran";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(162, 306);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(120, 19);
            this.metroLabel4.TabIndex = 18;
            this.metroLabel4.Text = "Status Pembayaran";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(164, 374);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(93, 19);
            this.metroLabel6.TabIndex = 20;
            this.metroLabel6.Text = "Total Terbilang";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(169, 431);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(50, 19);
            this.metroLabel7.TabIndex = 21;
            this.metroLabel7.Text = "Jumlah";
            // 
            // txtNama
            // 
            // 
            // 
            // 
            this.txtNama.CustomButton.Image = null;
            this.txtNama.CustomButton.Location = new System.Drawing.Point(207, 2);
            this.txtNama.CustomButton.Name = "";
            this.txtNama.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtNama.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNama.CustomButton.TabIndex = 1;
            this.txtNama.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNama.CustomButton.UseSelectable = true;
            this.txtNama.CustomButton.Visible = false;
            this.txtNama.Lines = new string[0];
            this.txtNama.Location = new System.Drawing.Point(315, 89);
            this.txtNama.MaxLength = 32767;
            this.txtNama.Name = "txtNama";
            this.txtNama.PasswordChar = '\0';
            this.txtNama.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNama.SelectedText = "";
            this.txtNama.SelectionLength = 0;
            this.txtNama.SelectionStart = 0;
            this.txtNama.ShortcutsEnabled = true;
            this.txtNama.Size = new System.Drawing.Size(227, 22);
            this.txtNama.TabIndex = 22;
            this.txtNama.UseSelectable = true;
            this.txtNama.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNama.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtJurusan
            // 
            // 
            // 
            // 
            this.txtJurusan.CustomButton.Image = null;
            this.txtJurusan.CustomButton.Location = new System.Drawing.Point(202, 1);
            this.txtJurusan.CustomButton.Name = "";
            this.txtJurusan.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtJurusan.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtJurusan.CustomButton.TabIndex = 1;
            this.txtJurusan.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtJurusan.CustomButton.UseSelectable = true;
            this.txtJurusan.CustomButton.Visible = false;
            this.txtJurusan.Lines = new string[0];
            this.txtJurusan.Location = new System.Drawing.Point(315, 127);
            this.txtJurusan.MaxLength = 32767;
            this.txtJurusan.Name = "txtJurusan";
            this.txtJurusan.PasswordChar = '\0';
            this.txtJurusan.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtJurusan.SelectedText = "";
            this.txtJurusan.SelectionLength = 0;
            this.txtJurusan.SelectionStart = 0;
            this.txtJurusan.ShortcutsEnabled = true;
            this.txtJurusan.Size = new System.Drawing.Size(228, 27);
            this.txtJurusan.TabIndex = 23;
            this.txtJurusan.UseSelectable = true;
            this.txtJurusan.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtJurusan.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtPembayaran
            // 
            // 
            // 
            // 
            this.txtPembayaran.CustomButton.Image = null;
            this.txtPembayaran.CustomButton.Location = new System.Drawing.Point(204, 2);
            this.txtPembayaran.CustomButton.Name = "";
            this.txtPembayaran.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.txtPembayaran.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPembayaran.CustomButton.TabIndex = 1;
            this.txtPembayaran.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPembayaran.CustomButton.UseSelectable = true;
            this.txtPembayaran.CustomButton.Visible = false;
            this.txtPembayaran.Lines = new string[0];
            this.txtPembayaran.Location = new System.Drawing.Point(316, 300);
            this.txtPembayaran.MaxLength = 32767;
            this.txtPembayaran.Name = "txtPembayaran";
            this.txtPembayaran.PasswordChar = '\0';
            this.txtPembayaran.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPembayaran.SelectedText = "";
            this.txtPembayaran.SelectionLength = 0;
            this.txtPembayaran.SelectionStart = 0;
            this.txtPembayaran.ShortcutsEnabled = true;
            this.txtPembayaran.Size = new System.Drawing.Size(226, 24);
            this.txtPembayaran.TabIndex = 25;
            this.txtPembayaran.UseSelectable = true;
            this.txtPembayaran.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPembayaran.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtTotalTerbilang
            // 
            // 
            // 
            // 
            this.txtTotalTerbilang.CustomButton.Image = null;
            this.txtTotalTerbilang.CustomButton.Location = new System.Drawing.Point(202, 1);
            this.txtTotalTerbilang.CustomButton.Name = "";
            this.txtTotalTerbilang.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtTotalTerbilang.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtTotalTerbilang.CustomButton.TabIndex = 1;
            this.txtTotalTerbilang.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtTotalTerbilang.CustomButton.UseSelectable = true;
            this.txtTotalTerbilang.CustomButton.Visible = false;
            this.txtTotalTerbilang.Lines = new string[0];
            this.txtTotalTerbilang.Location = new System.Drawing.Point(314, 365);
            this.txtTotalTerbilang.MaxLength = 32767;
            this.txtTotalTerbilang.Name = "txtTotalTerbilang";
            this.txtTotalTerbilang.PasswordChar = '\0';
            this.txtTotalTerbilang.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTotalTerbilang.SelectedText = "";
            this.txtTotalTerbilang.SelectionLength = 0;
            this.txtTotalTerbilang.SelectionStart = 0;
            this.txtTotalTerbilang.ShortcutsEnabled = true;
            this.txtTotalTerbilang.Size = new System.Drawing.Size(228, 27);
            this.txtTotalTerbilang.TabIndex = 27;
            this.txtTotalTerbilang.UseSelectable = true;
            this.txtTotalTerbilang.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtTotalTerbilang.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtJumlah
            // 
            // 
            // 
            // 
            this.txtJumlah.CustomButton.Image = null;
            this.txtJumlah.CustomButton.Location = new System.Drawing.Point(205, 2);
            this.txtJumlah.CustomButton.Name = "";
            this.txtJumlah.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtJumlah.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtJumlah.CustomButton.TabIndex = 1;
            this.txtJumlah.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtJumlah.CustomButton.UseSelectable = true;
            this.txtJumlah.CustomButton.Visible = false;
            this.txtJumlah.Lines = new string[0];
            this.txtJumlah.Location = new System.Drawing.Point(315, 428);
            this.txtJumlah.MaxLength = 32767;
            this.txtJumlah.Name = "txtJumlah";
            this.txtJumlah.PasswordChar = '\0';
            this.txtJumlah.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtJumlah.SelectedText = "";
            this.txtJumlah.SelectionLength = 0;
            this.txtJumlah.SelectionStart = 0;
            this.txtJumlah.ShortcutsEnabled = true;
            this.txtJumlah.Size = new System.Drawing.Size(225, 22);
            this.txtJumlah.TabIndex = 28;
            this.txtJumlah.UseSelectable = true;
            this.txtJumlah.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtJumlah.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(609, 394);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(94, 33);
            this.btnPrint.TabIndex = 29;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseSelectable = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnMainMenu
            // 
            this.btnMainMenu.Location = new System.Drawing.Point(609, 446);
            this.btnMainMenu.Name = "btnMainMenu";
            this.btnMainMenu.Size = new System.Drawing.Size(93, 33);
            this.btnMainMenu.TabIndex = 30;
            this.btnMainMenu.Text = "Main Menu";
            this.btnMainMenu.UseSelectable = true;
            this.btnMainMenu.Click += new System.EventHandler(this.btnMainMenu_Click);
            // 
            // txtTanggal
            // 
            // 
            // 
            // 
            this.txtTanggal.CustomButton.Image = null;
            this.txtTanggal.CustomButton.Location = new System.Drawing.Point(205, 1);
            this.txtTanggal.CustomButton.Name = "";
            this.txtTanggal.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtTanggal.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtTanggal.CustomButton.TabIndex = 1;
            this.txtTanggal.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtTanggal.CustomButton.UseSelectable = true;
            this.txtTanggal.CustomButton.Visible = false;
            this.txtTanggal.Lines = new string[0];
            this.txtTanggal.Location = new System.Drawing.Point(315, 235);
            this.txtTanggal.MaxLength = 32767;
            this.txtTanggal.Name = "txtTanggal";
            this.txtTanggal.PasswordChar = '\0';
            this.txtTanggal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTanggal.SelectedText = "";
            this.txtTanggal.SelectionLength = 0;
            this.txtTanggal.SelectionStart = 0;
            this.txtTanggal.ShortcutsEnabled = true;
            this.txtTanggal.Size = new System.Drawing.Size(227, 23);
            this.txtTanggal.TabIndex = 31;
            this.txtTanggal.UseSelectable = true;
            this.txtTanggal.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtTanggal.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtsemester
            // 
            // 
            // 
            // 
            this.txtsemester.CustomButton.Image = null;
            this.txtsemester.CustomButton.Location = new System.Drawing.Point(202, 1);
            this.txtsemester.CustomButton.Name = "";
            this.txtsemester.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtsemester.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtsemester.CustomButton.TabIndex = 1;
            this.txtsemester.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtsemester.CustomButton.UseSelectable = true;
            this.txtsemester.CustomButton.Visible = false;
            this.txtsemester.Lines = new string[0];
            this.txtsemester.Location = new System.Drawing.Point(312, 183);
            this.txtsemester.MaxLength = 32767;
            this.txtsemester.Name = "txtsemester";
            this.txtsemester.PasswordChar = '\0';
            this.txtsemester.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtsemester.SelectedText = "";
            this.txtsemester.SelectionLength = 0;
            this.txtsemester.SelectionStart = 0;
            this.txtsemester.ShortcutsEnabled = true;
            this.txtsemester.Size = new System.Drawing.Size(228, 27);
            this.txtsemester.TabIndex = 33;
            this.txtsemester.UseSelectable = true;
            this.txtsemester.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtsemester.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(160, 191);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(63, 19);
            this.metroLabel5.TabIndex = 32;
            this.metroLabel5.Text = "Semester";
            // 
            // CetakBuktiPembayaran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 565);
            this.Controls.Add(this.txtsemester);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.txtTanggal);
            this.Controls.Add(this.btnMainMenu);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.txtJumlah);
            this.Controls.Add(this.txtTotalTerbilang);
            this.Controls.Add(this.txtPembayaran);
            this.Controls.Add(this.txtJurusan);
            this.Controls.Add(this.txtNama);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "CetakBuktiPembayaran";
            this.Text = "CetakBuktiPembayaran";
            this.Load += new System.EventHandler(this.CetakBuktiPembayaran_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox txtJurusan;
        private MetroFramework.Controls.MetroTextBox txtPembayaran;
        private MetroFramework.Controls.MetroButton btnPrint;
        private MetroFramework.Controls.MetroButton btnMainMenu;
        private MetroFramework.Controls.MetroTextBox txtJumlah;
        private MetroFramework.Controls.MetroTextBox txtTotalTerbilang;
        private MetroFramework.Controls.MetroTextBox txtNama;
        private MetroFramework.Controls.MetroTextBox txtTanggal;
        private MetroFramework.Controls.MetroTextBox txtsemester;
        private MetroFramework.Controls.MetroLabel metroLabel5;
    }
}