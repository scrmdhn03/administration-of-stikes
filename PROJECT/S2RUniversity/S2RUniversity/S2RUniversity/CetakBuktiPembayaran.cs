﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.IO;

namespace S2RUniversity
{
    public partial class CetakBuktiPembayaran : MetroForm
    {
        public CetakBuktiPembayaran()
        {
            InitializeComponent();
        }

        private void txtNama_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtProgram_Click(object sender, EventArgs e)
        {

        }

        private void CetakBuktiPembayaran_Load(object sender, EventArgs e)
        {
            txtNama.Text = DataPembayaran.NamaLengkap;
            txtJurusan.Text = DataPembayaran.Jurusan;
            txtPembayaran.Text = DataPembayaran.StatusPembayaran;
            txtJumlah.Text = DataPembayaran.JumlahUang;
            txtTanggal.Text = DataPembayaran.TanggalPembayaran;
            txtsemester.Text = DataPembayaran.Semester;
        }

        private void btnMainMenu_Click(object sender, EventArgs e)
        {
            MenuAdmin obj = new MenuAdmin();
            obj.Show();
            this.Hide();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

        }
    }
}
