﻿namespace S2RUniversity
{
    partial class DaftarPendaftaran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroRadioButton1 = new MetroFramework.Controls.MetroRadioButton();
            this.btnDelete = new MetroFramework.Controls.MetroButton();
            this.btnupdate = new MetroFramework.Controls.MetroButton();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.dgMahasiswa = new MetroFramework.Controls.MetroGrid();
            this.CNIM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTTL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColJurusan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTahunAjaran = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColSemester = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CJK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAlamat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNoTelp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CStatusBayar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNoTelp = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtAlamat = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtNamaLengkap = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtNIM = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.cbJenisKelamin = new MetroFramework.Controls.MetroComboBox();
            this.mcbJurusan = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.mcbTahunAjaran = new MetroFramework.Controls.MetroComboBox();
            this.mcbSemester = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.txtStatusBayar = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.btnMenuAdmin = new MetroFramework.Controls.MetroButton();
            this.txtTanggalLahir = new MetroFramework.Controls.MetroTextBox();
            this.Report = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgMahasiswa)).BeginInit();
            this.SuspendLayout();
            // 
            // metroRadioButton1
            // 
            this.metroRadioButton1.AutoSize = true;
            this.metroRadioButton1.Location = new System.Drawing.Point(184, -17);
            this.metroRadioButton1.Name = "metroRadioButton1";
            this.metroRadioButton1.Size = new System.Drawing.Size(127, 15);
            this.metroRadioButton1.TabIndex = 39;
            this.metroRadioButton1.Text = "metroRadioButton1";
            this.metroRadioButton1.UseSelectable = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(549, 349);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(102, 39);
            this.btnDelete.TabIndex = 38;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseSelectable = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnupdate
            // 
            this.btnupdate.Location = new System.Drawing.Point(421, 352);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(102, 36);
            this.btnupdate.TabIndex = 37;
            this.btnupdate.Text = "Update";
            this.btnupdate.UseSelectable = true;
            this.btnupdate.Click += new System.EventHandler(this.btnupdate_Click);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(301, -26);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(137, 19);
            this.metroLabel8.TabIndex = 36;
            this.metroLabel8.Text = "Data Mahasiswa Baru ";
            // 
            // dgMahasiswa
            // 
            this.dgMahasiswa.AllowUserToResizeRows = false;
            this.dgMahasiswa.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgMahasiswa.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgMahasiswa.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgMahasiswa.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgMahasiswa.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgMahasiswa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMahasiswa.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CNIM,
            this.CNama,
            this.CTTL,
            this.ColJurusan,
            this.ColTahunAjaran,
            this.ColSemester,
            this.CJK,
            this.CAlamat,
            this.CNoTelp,
            this.CStatusBayar});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgMahasiswa.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgMahasiswa.EnableHeadersVisualStyles = false;
            this.dgMahasiswa.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgMahasiswa.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgMahasiswa.Location = new System.Drawing.Point(6, 413);
            this.dgMahasiswa.Name = "dgMahasiswa";
            this.dgMahasiswa.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgMahasiswa.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgMahasiswa.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgMahasiswa.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMahasiswa.Size = new System.Drawing.Size(896, 135);
            this.dgMahasiswa.TabIndex = 35;
            this.dgMahasiswa.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMahasiswa_CellContentClick);
            // 
            // CNIM
            // 
            this.CNIM.HeaderText = "NIM";
            this.CNIM.Name = "CNIM";
            // 
            // CNama
            // 
            this.CNama.HeaderText = "Nama";
            this.CNama.Name = "CNama";
            // 
            // CTTL
            // 
            this.CTTL.HeaderText = "Tanggal Lahir";
            this.CTTL.Name = "CTTL";
            // 
            // ColJurusan
            // 
            this.ColJurusan.HeaderText = "Jurusan";
            this.ColJurusan.Name = "ColJurusan";
            // 
            // ColTahunAjaran
            // 
            this.ColTahunAjaran.HeaderText = "Tahun Ajaran";
            this.ColTahunAjaran.Name = "ColTahunAjaran";
            // 
            // ColSemester
            // 
            this.ColSemester.HeaderText = "Semester";
            this.ColSemester.Name = "ColSemester";
            // 
            // CJK
            // 
            this.CJK.HeaderText = "Jenis Kelamin";
            this.CJK.Name = "CJK";
            // 
            // CAlamat
            // 
            this.CAlamat.HeaderText = "Alamat";
            this.CAlamat.Name = "CAlamat";
            // 
            // CNoTelp
            // 
            this.CNoTelp.HeaderText = "No Telp";
            this.CNoTelp.Name = "CNoTelp";
            // 
            // CStatusBayar
            // 
            this.CStatusBayar.HeaderText = "Status Bayar";
            this.CStatusBayar.Name = "CStatusBayar";
            // 
            // txtNoTelp
            // 
            // 
            // 
            // 
            this.txtNoTelp.CustomButton.Image = null;
            this.txtNoTelp.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtNoTelp.CustomButton.Name = "";
            this.txtNoTelp.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNoTelp.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNoTelp.CustomButton.TabIndex = 1;
            this.txtNoTelp.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNoTelp.CustomButton.UseSelectable = true;
            this.txtNoTelp.CustomButton.Visible = false;
            this.txtNoTelp.Lines = new string[0];
            this.txtNoTelp.Location = new System.Drawing.Point(652, 225);
            this.txtNoTelp.MaxLength = 32767;
            this.txtNoTelp.Name = "txtNoTelp";
            this.txtNoTelp.PasswordChar = '\0';
            this.txtNoTelp.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNoTelp.SelectedText = "";
            this.txtNoTelp.SelectionLength = 0;
            this.txtNoTelp.SelectionStart = 0;
            this.txtNoTelp.ShortcutsEnabled = true;
            this.txtNoTelp.Size = new System.Drawing.Size(181, 23);
            this.txtNoTelp.TabIndex = 32;
            this.txtNoTelp.UseSelectable = true;
            this.txtNoTelp.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNoTelp.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(499, 228);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(57, 19);
            this.metroLabel6.TabIndex = 31;
            this.metroLabel6.Text = "No. Telp";
            // 
            // txtAlamat
            // 
            // 
            // 
            // 
            this.txtAlamat.CustomButton.Image = null;
            this.txtAlamat.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtAlamat.CustomButton.Name = "";
            this.txtAlamat.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtAlamat.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAlamat.CustomButton.TabIndex = 1;
            this.txtAlamat.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAlamat.CustomButton.UseSelectable = true;
            this.txtAlamat.CustomButton.Visible = false;
            this.txtAlamat.Lines = new string[0];
            this.txtAlamat.Location = new System.Drawing.Point(652, 173);
            this.txtAlamat.MaxLength = 32767;
            this.txtAlamat.Name = "txtAlamat";
            this.txtAlamat.PasswordChar = '\0';
            this.txtAlamat.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAlamat.SelectedText = "";
            this.txtAlamat.SelectionLength = 0;
            this.txtAlamat.SelectionStart = 0;
            this.txtAlamat.ShortcutsEnabled = true;
            this.txtAlamat.Size = new System.Drawing.Size(181, 23);
            this.txtAlamat.TabIndex = 30;
            this.txtAlamat.UseSelectable = true;
            this.txtAlamat.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAlamat.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(499, 180);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(51, 19);
            this.metroLabel5.TabIndex = 29;
            this.metroLabel5.Text = "Alamat";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(499, 130);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(86, 19);
            this.metroLabel4.TabIndex = 27;
            this.metroLabel4.Text = "Jenis Kelamin";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(36, 208);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(86, 19);
            this.metroLabel3.TabIndex = 25;
            this.metroLabel3.Text = "Tanggal Lahir";
            // 
            // txtNamaLengkap
            // 
            // 
            // 
            // 
            this.txtNamaLengkap.CustomButton.Image = null;
            this.txtNamaLengkap.CustomButton.Location = new System.Drawing.Point(160, 1);
            this.txtNamaLengkap.CustomButton.Name = "";
            this.txtNamaLengkap.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNamaLengkap.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNamaLengkap.CustomButton.TabIndex = 1;
            this.txtNamaLengkap.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNamaLengkap.CustomButton.UseSelectable = true;
            this.txtNamaLengkap.CustomButton.Visible = false;
            this.txtNamaLengkap.Lines = new string[0];
            this.txtNamaLengkap.Location = new System.Drawing.Point(214, 169);
            this.txtNamaLengkap.MaxLength = 32767;
            this.txtNamaLengkap.Name = "txtNamaLengkap";
            this.txtNamaLengkap.PasswordChar = '\0';
            this.txtNamaLengkap.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNamaLengkap.SelectedText = "";
            this.txtNamaLengkap.SelectionLength = 0;
            this.txtNamaLengkap.SelectionStart = 0;
            this.txtNamaLengkap.ShortcutsEnabled = true;
            this.txtNamaLengkap.Size = new System.Drawing.Size(182, 23);
            this.txtNamaLengkap.TabIndex = 24;
            this.txtNamaLengkap.UseSelectable = true;
            this.txtNamaLengkap.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNamaLengkap.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNamaLengkap.Click += new System.EventHandler(this.txtNamaLengkap_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(36, 168);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(98, 19);
            this.metroLabel2.TabIndex = 23;
            this.metroLabel2.Text = "Nama Lengkap";
            // 
            // txtNIM
            // 
            // 
            // 
            // 
            this.txtNIM.CustomButton.Image = null;
            this.txtNIM.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtNIM.CustomButton.Name = "";
            this.txtNIM.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNIM.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNIM.CustomButton.TabIndex = 1;
            this.txtNIM.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNIM.CustomButton.UseSelectable = true;
            this.txtNIM.CustomButton.Visible = false;
            this.txtNIM.Lines = new string[0];
            this.txtNIM.Location = new System.Drawing.Point(214, 130);
            this.txtNIM.MaxLength = 32767;
            this.txtNIM.Name = "txtNIM";
            this.txtNIM.PasswordChar = '\0';
            this.txtNIM.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNIM.SelectedText = "";
            this.txtNIM.SelectionLength = 0;
            this.txtNIM.SelectionStart = 0;
            this.txtNIM.ShortcutsEnabled = true;
            this.txtNIM.Size = new System.Drawing.Size(181, 23);
            this.txtNIM.TabIndex = 22;
            this.txtNIM.UseSelectable = true;
            this.txtNIM.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNIM.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNIM.Click += new System.EventHandler(this.txtNIM_Click);
            this.txtNIM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNIM_KeyDown);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(36, 125);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(34, 19);
            this.metroLabel1.TabIndex = 21;
            this.metroLabel1.Text = "NIM";
            this.metroLabel1.Click += new System.EventHandler(this.metroLabel1_Click);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(403, 60);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(153, 19);
            this.metroLabel9.TabIndex = 40;
            this.metroLabel9.Text = "Daftar mahasiswa STIKES";
            this.metroLabel9.Click += new System.EventHandler(this.metroLabel9_Click);
            // 
            // cbJenisKelamin
            // 
            this.cbJenisKelamin.FormattingEnabled = true;
            this.cbJenisKelamin.ItemHeight = 23;
            this.cbJenisKelamin.Items.AddRange(new object[] {
            "Perempuan",
            "Laki-Laki"});
            this.cbJenisKelamin.Location = new System.Drawing.Point(652, 124);
            this.cbJenisKelamin.Name = "cbJenisKelamin";
            this.cbJenisKelamin.Size = new System.Drawing.Size(174, 29);
            this.cbJenisKelamin.TabIndex = 41;
            this.cbJenisKelamin.UseSelectable = true;
            // 
            // mcbJurusan
            // 
            this.mcbJurusan.FormattingEnabled = true;
            this.mcbJurusan.ItemHeight = 23;
            this.mcbJurusan.Items.AddRange(new object[] {
            "Gizi",
            "Keperawatan",
            "Kebidananan",
            "Kesehatan Lingkungan"});
            this.mcbJurusan.Location = new System.Drawing.Point(212, 253);
            this.mcbJurusan.Name = "mcbJurusan";
            this.mcbJurusan.Size = new System.Drawing.Size(183, 29);
            this.mcbJurusan.TabIndex = 45;
            this.mcbJurusan.UseSelectable = true;
            this.mcbJurusan.SelectedIndexChanged += new System.EventHandler(this.mcbJurusan_SelectedIndexChanged);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(36, 253);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(52, 19);
            this.metroLabel10.TabIndex = 44;
            this.metroLabel10.Text = "Jurusan";
            // 
            // mcbTahunAjaran
            // 
            this.mcbTahunAjaran.FormattingEnabled = true;
            this.mcbTahunAjaran.ItemHeight = 23;
            this.mcbTahunAjaran.Items.AddRange(new object[] {
            "2015-2016",
            "2016-2017",
            "2017-2018",
            "2018-2019",
            "",
            ""});
            this.mcbTahunAjaran.Location = new System.Drawing.Point(212, 302);
            this.mcbTahunAjaran.Name = "mcbTahunAjaran";
            this.mcbTahunAjaran.Size = new System.Drawing.Size(183, 29);
            this.mcbTahunAjaran.TabIndex = 49;
            this.mcbTahunAjaran.UseSelectable = true;
            // 
            // mcbSemester
            // 
            this.mcbSemester.FormattingEnabled = true;
            this.mcbSemester.ItemHeight = 23;
            this.mcbSemester.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.mcbSemester.Location = new System.Drawing.Point(212, 349);
            this.mcbSemester.Name = "mcbSemester";
            this.mcbSemester.Size = new System.Drawing.Size(183, 29);
            this.mcbSemester.TabIndex = 48;
            this.mcbSemester.UseSelectable = true;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(36, 352);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(63, 19);
            this.metroLabel11.TabIndex = 47;
            this.metroLabel11.Text = "Semester";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(36, 302);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(84, 19);
            this.metroLabel12.TabIndex = 46;
            this.metroLabel12.Text = "Tahun Ajaran";
            // 
            // txtStatusBayar
            // 
            this.txtStatusBayar.FormattingEnabled = true;
            this.txtStatusBayar.ItemHeight = 23;
            this.txtStatusBayar.Items.AddRange(new object[] {
            "Lunas",
            "Belum Lunas"});
            this.txtStatusBayar.Location = new System.Drawing.Point(653, 269);
            this.txtStatusBayar.Name = "txtStatusBayar";
            this.txtStatusBayar.Size = new System.Drawing.Size(180, 29);
            this.txtStatusBayar.TabIndex = 52;
            this.txtStatusBayar.UseSelectable = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(497, 274);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(80, 19);
            this.metroLabel7.TabIndex = 51;
            this.metroLabel7.Text = "Status Bayar";
            // 
            // btnMenuAdmin
            // 
            this.btnMenuAdmin.Location = new System.Drawing.Point(674, 349);
            this.btnMenuAdmin.Name = "btnMenuAdmin";
            this.btnMenuAdmin.Size = new System.Drawing.Size(111, 40);
            this.btnMenuAdmin.TabIndex = 53;
            this.btnMenuAdmin.Text = "Menu Admin";
            this.btnMenuAdmin.UseSelectable = true;
            this.btnMenuAdmin.Click += new System.EventHandler(this.btnMainMenu_Click);
            // 
            // txtTanggalLahir
            // 
            // 
            // 
            // 
            this.txtTanggalLahir.CustomButton.Image = null;
            this.txtTanggalLahir.CustomButton.Location = new System.Drawing.Point(162, 1);
            this.txtTanggalLahir.CustomButton.Name = "";
            this.txtTanggalLahir.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtTanggalLahir.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtTanggalLahir.CustomButton.TabIndex = 1;
            this.txtTanggalLahir.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtTanggalLahir.CustomButton.UseSelectable = true;
            this.txtTanggalLahir.CustomButton.Visible = false;
            this.txtTanggalLahir.Lines = new string[0];
            this.txtTanggalLahir.Location = new System.Drawing.Point(212, 219);
            this.txtTanggalLahir.MaxLength = 32767;
            this.txtTanggalLahir.Name = "txtTanggalLahir";
            this.txtTanggalLahir.PasswordChar = '\0';
            this.txtTanggalLahir.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTanggalLahir.SelectedText = "";
            this.txtTanggalLahir.SelectionLength = 0;
            this.txtTanggalLahir.SelectionStart = 0;
            this.txtTanggalLahir.ShortcutsEnabled = true;
            this.txtTanggalLahir.Size = new System.Drawing.Size(184, 23);
            this.txtTanggalLahir.TabIndex = 54;
            this.txtTanggalLahir.UseSelectable = true;
            this.txtTanggalLahir.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtTanggalLahir.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Report
            // 
            this.Report.Location = new System.Drawing.Point(802, 352);
            this.Report.Name = "Report";
            this.Report.Size = new System.Drawing.Size(111, 40);
            this.Report.TabIndex = 55;
            this.Report.Text = "Report";
            this.Report.UseSelectable = true;
            this.Report.Click += new System.EventHandler(this.Report_Click);
            // 
            // DaftarPendaftaran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 605);
            this.Controls.Add(this.Report);
            this.Controls.Add(this.txtTanggalLahir);
            this.Controls.Add(this.btnMenuAdmin);
            this.Controls.Add(this.txtStatusBayar);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.mcbTahunAjaran);
            this.Controls.Add(this.mcbSemester);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.mcbJurusan);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.cbJenisKelamin);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroRadioButton1);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnupdate);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.dgMahasiswa);
            this.Controls.Add(this.txtNoTelp);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.txtAlamat);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.txtNamaLengkap);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.txtNIM);
            this.Controls.Add(this.metroLabel1);
            this.Name = "DaftarPendaftaran";
            this.Text = "DaftarPendaftaran";
            this.Load += new System.EventHandler(this.DaftarPendaftaran_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgMahasiswa)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroRadioButton metroRadioButton1;
        private MetroFramework.Controls.MetroButton btnDelete;
        private MetroFramework.Controls.MetroButton btnupdate;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroGrid dgMahasiswa;
        private MetroFramework.Controls.MetroTextBox txtNoTelp;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtAlamat;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtNamaLengkap;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtNIM;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroComboBox cbJenisKelamin;
        private MetroFramework.Controls.MetroComboBox mcbJurusan;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroComboBox mcbTahunAjaran;
        private MetroFramework.Controls.MetroComboBox mcbSemester;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroComboBox txtStatusBayar;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroButton btnMenuAdmin;
        private System.Windows.Forms.DataGridViewTextBoxColumn CStatusBayar;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNoTelp;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAlamat;
        private System.Windows.Forms.DataGridViewTextBoxColumn CJK;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColSemester;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTahunAjaran;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColJurusan;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTTL;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNama;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNIM;
        private MetroFramework.Controls.MetroTextBox txtTanggalLahir;
        private MetroFramework.Controls.MetroButton Report;
    }
}