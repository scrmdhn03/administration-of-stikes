﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.IO;

namespace S2RUniversity
{
    public partial class DaftarPendaftaran : MetroForm
    {
        public DaftarPendaftaran()
        {
            InitializeComponent();
        }

        private void metroLabel9_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel1_Click(object sender, EventArgs e)
        {

        }

        private void txtNIM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {// kalo di enter jadi langsung masuk ke yg baru di cari gak kosong dl
                try
                {
                    dgMahasiswa.Rows.Clear();
                    var Lines = File.ReadAllLines("DaftarMahasiswa2.txt");
                    if (Lines.Count() > 0)
                    {
                        foreach (var cellArray in Lines)
                        {
                            var cell = cellArray.Split('#');

                            if (cell.Contains(txtNIM.Text))
                            {
                                if (cell.Length == dgMahasiswa.Columns.Count)
                                    dgMahasiswa.Rows.Add(cell);
                                txtNamaLengkap.Text = cell[1];
                                txtTanggalLahir.Text = cell[2];
                                mcbJurusan.Text = cell[3];
                                mcbTahunAjaran.Text = cell[4];
                                mcbSemester.Text = cell[5];
                                cbJenisKelamin.Text = cell[6];
                                txtAlamat.Text = cell[7];
                                txtNoTelp.Text = cell[8];
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public void Display()
        {
            dgMahasiswa.Rows.Clear();
            var Lines = File.ReadAllLines("DaftarMahasiswa2.txt");
            if (Lines.Count() > 0)
            {
                foreach (var cellArray in Lines)
                {
                    var cell = cellArray.Split('#');
                    if (cell.Length == dgMahasiswa.Columns.Count)

                        dgMahasiswa.Rows.Add(cell);
                }
            }

        }

        private void DaftarPendaftaran_Load(object sender, EventArgs e)
        {
            Display();
        }

        private void txtNIM_Click(object sender, EventArgs e)
        {
            //kenapa pas mau validasi NIM error 
        }

        private void txtJenisKelamin_Click(object sender, EventArgs e)
        {

        }

        private void dgMahasiswa_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder newFile = new StringBuilder();
                string temp = "";
                dgMahasiswa.Rows.Clear();
                var lines = File.ReadAllLines("DaftarMahasiswa2.txt");
                foreach (var cellArray in lines)
                {
                    if (cellArray.Contains(txtNIM.Text))
                    {
                        temp = cellArray.Remove(0);
                        continue;

                    }
                    newFile.Append(cellArray + "\r\n");

                }
                File.WriteAllText("DaftarMahasiswa2.txt", newFile.ToString());
                MessageBox.Show("Data Has been deleted successfully");
                Display();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> newfile = new List<string>();
                //string temp = "";
                dgMahasiswa.Rows.Clear();
                var lines = File.ReadAllLines("DaftarMahasiswa2.txt");

                foreach (var cellArray in lines)
                {
                    var data = cellArray.Split('#');
                    if (data[0].Equals(txtNIM.Text))
                    {
                        data[1] = txtNamaLengkap.Text.Trim(); //jika yg di ubah banyak ini nya banyak
                        data[2] = txtTanggalLahir.Text;
                        //data[3] = mdTanggalLahir.Text;
                        data[3] = mcbJurusan.Text;
                        data[4] = mcbTahunAjaran.Text;
                        data[5] = mcbSemester.Text;
                        data[6] = cbJenisKelamin.Text;
                        data[7] = txtAlamat.Text;
                        data[8] = txtNoTelp.Text;
                        data[9] = txtStatusBayar.Text;
                    }
                    newfile.Add(string.Join("#", data));
                }
                File.WriteAllLines("DaftarMahasiswa2.txt", newfile);

                MessageBox.Show("Data has been updated");
                Display();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void txtJurusan_Click(object sender, EventArgs e)
        {

        }

 

        private void mdTanggalLahir_ValueChanged(object sender, EventArgs e)
        {

        }

        private void mcbJurusan_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnMainMenu_Click(object sender, EventArgs e)
        {
            MenuAdmin obj = new MenuAdmin();
            obj.Show();
            this.Hide();
        }

        private void txtNamaLengkap_Click(object sender, EventArgs e)
        {

        }

        private void Report_Click(object sender, EventArgs e)
        {
            RptMahasiswa obj = new RptMahasiswa();
            obj.Show();
        }
    }
}