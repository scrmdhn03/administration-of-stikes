﻿namespace S2RUniversity
{
    partial class DataMABA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.btnDelete = new MetroFramework.Controls.MetroButton();
            this.btnupdate = new MetroFramework.Controls.MetroButton();
            this.dgD3 = new MetroFramework.Controls.MetroGrid();
            this.CNISN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTTL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CJK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAgama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAlamat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNoTelp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNamaOrtu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAlamatOrtu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAsalSekolah = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CJurusan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CStatusbayar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtJurusan = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.txtNoTelp = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtAlamat = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.txtJenisKelamin = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtTTL = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtNama = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtNISN = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.cStatus = new MetroFramework.Controls.MetroComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgD3)).BeginInit();
            this.SuspendLayout();
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(619, 416);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(75, 19);
            this.metroButton1.TabIndex = 40;
            this.metroButton1.Text = "Report";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(602, 304);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(89, 19);
            this.btnBack.TabIndex = 39;
            this.btnBack.Text = "Back To Menu";
            this.btnBack.UseSelectable = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(619, 383);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 19);
            this.btnDelete.TabIndex = 38;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseSelectable = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnupdate
            // 
            this.btnupdate.Location = new System.Drawing.Point(616, 341);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(75, 19);
            this.btnupdate.TabIndex = 37;
            this.btnupdate.Text = "Update";
            this.btnupdate.UseSelectable = true;
            this.btnupdate.Click += new System.EventHandler(this.btnupdate_Click);
            // 
            // dgD3
            // 
            this.dgD3.AllowUserToResizeRows = false;
            this.dgD3.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgD3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgD3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgD3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgD3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgD3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgD3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CNISN,
            this.CNama,
            this.CTTL,
            this.CJK,
            this.CAgama,
            this.CAlamat,
            this.CNoTelp,
            this.CNamaOrtu,
            this.CAlamatOrtu,
            this.CAsalSekolah,
            this.CJurusan,
            this.CStatusbayar});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgD3.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgD3.EnableHeadersVisualStyles = false;
            this.dgD3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgD3.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgD3.Location = new System.Drawing.Point(85, 304);
            this.dgD3.Name = "dgD3";
            this.dgD3.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgD3.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgD3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgD3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgD3.Size = new System.Drawing.Size(434, 131);
            this.dgD3.TabIndex = 36;
            // 
            // CNISN
            // 
            this.CNISN.HeaderText = "NISN";
            this.CNISN.Name = "CNISN";
            // 
            // CNama
            // 
            this.CNama.HeaderText = "Nama";
            this.CNama.Name = "CNama";
            // 
            // CTTL
            // 
            this.CTTL.HeaderText = "Tanggal Lahir";
            this.CTTL.Name = "CTTL";
            // 
            // CJK
            // 
            this.CJK.HeaderText = "Jenis Kelamin";
            this.CJK.Name = "CJK";
            // 
            // CAgama
            // 
            this.CAgama.HeaderText = "Agama";
            this.CAgama.Name = "CAgama";
            // 
            // CAlamat
            // 
            this.CAlamat.HeaderText = "Alamat";
            this.CAlamat.Name = "CAlamat";
            // 
            // CNoTelp
            // 
            this.CNoTelp.HeaderText = "No Telp";
            this.CNoTelp.Name = "CNoTelp";
            // 
            // CNamaOrtu
            // 
            this.CNamaOrtu.HeaderText = "Nama Orang Tua";
            this.CNamaOrtu.Name = "CNamaOrtu";
            // 
            // CAlamatOrtu
            // 
            this.CAlamatOrtu.HeaderText = "Alamat Orang Tua";
            this.CAlamatOrtu.Name = "CAlamatOrtu";
            // 
            // CAsalSekolah
            // 
            this.CAsalSekolah.HeaderText = "Asal Sekolah";
            this.CAsalSekolah.Name = "CAsalSekolah";
            // 
            // CJurusan
            // 
            this.CJurusan.HeaderText = "Jurusan";
            this.CJurusan.Name = "CJurusan";
            // 
            // CStatusbayar
            // 
            this.CStatusbayar.HeaderText = "Status Bayar";
            this.CStatusbayar.Name = "CStatusbayar";
            // 
            // txtJurusan
            // 
            // 
            // 
            // 
            this.txtJurusan.CustomButton.Image = null;
            this.txtJurusan.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtJurusan.CustomButton.Name = "";
            this.txtJurusan.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtJurusan.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtJurusan.CustomButton.TabIndex = 1;
            this.txtJurusan.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtJurusan.CustomButton.UseSelectable = true;
            this.txtJurusan.CustomButton.Visible = false;
            this.txtJurusan.Lines = new string[0];
            this.txtJurusan.Location = new System.Drawing.Point(503, 207);
            this.txtJurusan.MaxLength = 32767;
            this.txtJurusan.Name = "txtJurusan";
            this.txtJurusan.PasswordChar = '\0';
            this.txtJurusan.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtJurusan.SelectedText = "";
            this.txtJurusan.SelectionLength = 0;
            this.txtJurusan.SelectionStart = 0;
            this.txtJurusan.ShortcutsEnabled = true;
            this.txtJurusan.Size = new System.Drawing.Size(181, 23);
            this.txtJurusan.TabIndex = 35;
            this.txtJurusan.UseSelectable = true;
            this.txtJurusan.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtJurusan.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(394, 207);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(99, 19);
            this.metroLabel7.TabIndex = 34;
            this.metroLabel7.Text = "Jurusan           :";
            // 
            // txtNoTelp
            // 
            // 
            // 
            // 
            this.txtNoTelp.CustomButton.Image = null;
            this.txtNoTelp.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtNoTelp.CustomButton.Name = "";
            this.txtNoTelp.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNoTelp.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNoTelp.CustomButton.TabIndex = 1;
            this.txtNoTelp.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNoTelp.CustomButton.UseSelectable = true;
            this.txtNoTelp.CustomButton.Visible = false;
            this.txtNoTelp.Lines = new string[0];
            this.txtNoTelp.Location = new System.Drawing.Point(500, 158);
            this.txtNoTelp.MaxLength = 32767;
            this.txtNoTelp.Name = "txtNoTelp";
            this.txtNoTelp.PasswordChar = '\0';
            this.txtNoTelp.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNoTelp.SelectedText = "";
            this.txtNoTelp.SelectionLength = 0;
            this.txtNoTelp.SelectionStart = 0;
            this.txtNoTelp.ShortcutsEnabled = true;
            this.txtNoTelp.Size = new System.Drawing.Size(181, 23);
            this.txtNoTelp.TabIndex = 33;
            this.txtNoTelp.UseSelectable = true;
            this.txtNoTelp.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNoTelp.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(391, 158);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(100, 19);
            this.metroLabel6.TabIndex = 32;
            this.metroLabel6.Text = "No. Telp          :";
            // 
            // txtAlamat
            // 
            // 
            // 
            // 
            this.txtAlamat.CustomButton.Image = null;
            this.txtAlamat.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtAlamat.CustomButton.Name = "";
            this.txtAlamat.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtAlamat.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAlamat.CustomButton.TabIndex = 1;
            this.txtAlamat.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAlamat.CustomButton.UseSelectable = true;
            this.txtAlamat.CustomButton.Visible = false;
            this.txtAlamat.Lines = new string[0];
            this.txtAlamat.Location = new System.Drawing.Point(500, 110);
            this.txtAlamat.MaxLength = 32767;
            this.txtAlamat.Name = "txtAlamat";
            this.txtAlamat.PasswordChar = '\0';
            this.txtAlamat.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAlamat.SelectedText = "";
            this.txtAlamat.SelectionLength = 0;
            this.txtAlamat.SelectionStart = 0;
            this.txtAlamat.ShortcutsEnabled = true;
            this.txtAlamat.Size = new System.Drawing.Size(181, 23);
            this.txtAlamat.TabIndex = 31;
            this.txtAlamat.UseSelectable = true;
            this.txtAlamat.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAlamat.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(391, 110);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(102, 19);
            this.metroLabel5.TabIndex = 30;
            this.metroLabel5.Text = "Alamat            :";
            // 
            // txtJenisKelamin
            // 
            // 
            // 
            // 
            this.txtJenisKelamin.CustomButton.Image = null;
            this.txtJenisKelamin.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtJenisKelamin.CustomButton.Name = "";
            this.txtJenisKelamin.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtJenisKelamin.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtJenisKelamin.CustomButton.TabIndex = 1;
            this.txtJenisKelamin.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtJenisKelamin.CustomButton.UseSelectable = true;
            this.txtJenisKelamin.CustomButton.Visible = false;
            this.txtJenisKelamin.Lines = new string[0];
            this.txtJenisKelamin.Location = new System.Drawing.Point(152, 261);
            this.txtJenisKelamin.MaxLength = 32767;
            this.txtJenisKelamin.Name = "txtJenisKelamin";
            this.txtJenisKelamin.PasswordChar = '\0';
            this.txtJenisKelamin.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtJenisKelamin.SelectedText = "";
            this.txtJenisKelamin.SelectionLength = 0;
            this.txtJenisKelamin.SelectionStart = 0;
            this.txtJenisKelamin.ShortcutsEnabled = true;
            this.txtJenisKelamin.Size = new System.Drawing.Size(181, 23);
            this.txtJenisKelamin.TabIndex = 29;
            this.txtJenisKelamin.UseSelectable = true;
            this.txtJenisKelamin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtJenisKelamin.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(43, 265);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(109, 19);
            this.metroLabel4.TabIndex = 28;
            this.metroLabel4.Text = "Jenis Kelamin     :";
            // 
            // txtTTL
            // 
            // 
            // 
            // 
            this.txtTTL.CustomButton.Image = null;
            this.txtTTL.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtTTL.CustomButton.Name = "";
            this.txtTTL.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtTTL.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtTTL.CustomButton.TabIndex = 1;
            this.txtTTL.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtTTL.CustomButton.UseSelectable = true;
            this.txtTTL.CustomButton.Visible = false;
            this.txtTTL.Lines = new string[0];
            this.txtTTL.Location = new System.Drawing.Point(151, 219);
            this.txtTTL.MaxLength = 32767;
            this.txtTTL.Name = "txtTTL";
            this.txtTTL.PasswordChar = '\0';
            this.txtTTL.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTTL.SelectedText = "";
            this.txtTTL.SelectionLength = 0;
            this.txtTTL.SelectionStart = 0;
            this.txtTTL.ShortcutsEnabled = true;
            this.txtTTL.Size = new System.Drawing.Size(181, 23);
            this.txtTTL.TabIndex = 27;
            this.txtTTL.UseSelectable = true;
            this.txtTTL.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtTTL.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(43, 223);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(105, 19);
            this.metroLabel3.TabIndex = 26;
            this.metroLabel3.Text = "Tanggal Lahir    :";
            // 
            // txtNama
            // 
            // 
            // 
            // 
            this.txtNama.CustomButton.Image = null;
            this.txtNama.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtNama.CustomButton.Name = "";
            this.txtNama.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNama.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNama.CustomButton.TabIndex = 1;
            this.txtNama.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNama.CustomButton.UseSelectable = true;
            this.txtNama.CustomButton.Visible = false;
            this.txtNama.Lines = new string[0];
            this.txtNama.Location = new System.Drawing.Point(155, 174);
            this.txtNama.MaxLength = 32767;
            this.txtNama.Name = "txtNama";
            this.txtNama.PasswordChar = '\0';
            this.txtNama.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNama.SelectedText = "";
            this.txtNama.SelectionLength = 0;
            this.txtNama.SelectionStart = 0;
            this.txtNama.ShortcutsEnabled = true;
            this.txtNama.Size = new System.Drawing.Size(181, 23);
            this.txtNama.TabIndex = 25;
            this.txtNama.UseSelectable = true;
            this.txtNama.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNama.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(47, 174);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(108, 19);
            this.metroLabel2.TabIndex = 24;
            this.metroLabel2.Text = "Nama               :";
            // 
            // txtNISN
            // 
            // 
            // 
            // 
            this.txtNISN.CustomButton.Image = null;
            this.txtNISN.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtNISN.CustomButton.Name = "";
            this.txtNISN.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNISN.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNISN.CustomButton.TabIndex = 1;
            this.txtNISN.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNISN.CustomButton.UseSelectable = true;
            this.txtNISN.CustomButton.Visible = false;
            this.txtNISN.Lines = new string[0];
            this.txtNISN.Location = new System.Drawing.Point(156, 110);
            this.txtNISN.MaxLength = 32767;
            this.txtNISN.Name = "txtNISN";
            this.txtNISN.PasswordChar = '\0';
            this.txtNISN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNISN.SelectedText = "";
            this.txtNISN.SelectionLength = 0;
            this.txtNISN.SelectionStart = 0;
            this.txtNISN.ShortcutsEnabled = true;
            this.txtNISN.Size = new System.Drawing.Size(181, 23);
            this.txtNISN.TabIndex = 23;
            this.txtNISN.UseSelectable = true;
            this.txtNISN.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNISN.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNISN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNISN_KeyDown);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(47, 110);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(106, 19);
            this.metroLabel1.TabIndex = 22;
            this.metroLabel1.Text = "NISN                :";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(311, 60);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(137, 19);
            this.metroLabel8.TabIndex = 41;
            this.metroLabel8.Text = "Data Mahasiswa Baru ";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(391, 248);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(109, 19);
            this.metroLabel9.TabIndex = 42;
            this.metroLabel9.Text = "status Bayar       :";
            // 
            // cStatus
            // 
            this.cStatus.FormattingEnabled = true;
            this.cStatus.ItemHeight = 23;
            this.cStatus.Items.AddRange(new object[] {
            "Belum Bayar",
            "Sudah Bayar"});
            this.cStatus.Location = new System.Drawing.Point(500, 248);
            this.cStatus.Name = "cStatus";
            this.cStatus.Size = new System.Drawing.Size(184, 29);
            this.cStatus.TabIndex = 43;
            this.cStatus.UseSelectable = true;
            // 
            // DataMABA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 477);
            this.Controls.Add(this.cStatus);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnupdate);
            this.Controls.Add(this.dgD3);
            this.Controls.Add(this.txtJurusan);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.txtNoTelp);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.txtAlamat);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.txtJenisKelamin);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.txtTTL);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.txtNama);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.txtNISN);
            this.Controls.Add(this.metroLabel1);
            this.Name = "DataMABA";
            this.Text = "DataMABA";
            this.Load += new System.EventHandler(this.DataMABA_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgD3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtNISN;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtNama;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtTTL;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtJenisKelamin;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox txtAlamat;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtNoTelp;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox txtJurusan;
        private MetroFramework.Controls.MetroGrid dgD3;
        private MetroFramework.Controls.MetroButton btnupdate;
        private MetroFramework.Controls.MetroButton btnDelete;
        private MetroFramework.Controls.MetroButton btnBack;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroComboBox cStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn CStatusbayar;
        private System.Windows.Forms.DataGridViewTextBoxColumn CJurusan;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAsalSekolah;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAlamatOrtu;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNamaOrtu;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNoTelp;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAlamat;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAgama;
        private System.Windows.Forms.DataGridViewTextBoxColumn CJK;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTTL;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNama;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNISN;
    }
}