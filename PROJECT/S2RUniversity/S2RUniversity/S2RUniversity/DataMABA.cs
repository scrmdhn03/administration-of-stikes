﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.IO;

namespace S2RUniversity
{
    public partial class DataMABA : MetroForm
    {
        public DataMABA()
        {
            InitializeComponent();
        }

        public void Display()
        {
            dgD3.Rows.Clear();
            var Lines = File.ReadAllLines("DataPendaftarD3.txt");
            if (Lines.Count() > 0)
            {
                foreach (var cellArray in Lines)
                {
                    var cell = cellArray.Split('#');
                    if (cell.Length == dgD3.Columns.Count)

                        dgD3.Rows.Add(cell);
                }
            }

        }
        private void DataMABA_Load(object sender, EventArgs e)
        {
            Display();
        }

        private void txtNISN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) // kalo di enter jadi langsung masuk ke yg baru di cari gak kosong dl
                try
                {
                    dgD3.Rows.Clear();
                    var Lines = File.ReadAllLines("DataPendaftarD3.txt");
                    if (Lines.Count() > 0)
                    {
                        foreach (var cellArray in Lines)
                        {
                            var cell = cellArray.Split('#');

                            if (cell.Contains(txtNISN.Text))
                            {
                                if (cell.Length == dgD3.Columns.Count)
                                    dgD3.Rows.Add(cell);
                                txtNama.Text = cell[1];
                                txtTTL.Text = cell[2];
                                txtAlamat.Text = cell[3];
                                txtJenisKelamin.Text = cell[4];
                                txtNoTelp.Text = cell[6];
                                txtJurusan.Text = cell[10];
                                cStatus.Text = cell[11];

                            }


                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> newfile = new List<string>();
                string temp = "";
                dgD3.Rows.Clear();
                var lines = File.ReadAllLines("DataPendaftarD3.txt");

                foreach (var cellArray in lines)
                {
                    var data = cellArray.Split('#');
                    if (data[0].Equals(txtNISN.Text))
                    {
                        data[1] = txtNama.Text.Trim(); //jika yg di ubah banyak ini nya banyak
                        data[2] = txtTTL.Text;
                        data[4] = txtJenisKelamin.Text;
                        data[3] = txtAlamat.Text;
                        data[6] = txtNoTelp.Text;
                        data[10] = txtJurusan.Text;
                        data[11] = cStatus.Text;
                    }
                    newfile.Add(string.Join("#", data));
                }
                File.WriteAllLines("DataPendaftarD3.txt", newfile);

                MessageBox.Show("Data has been updated");
                Display();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder newFile = new StringBuilder();
                string temp = "";
                dgD3.Rows.Clear();
                var lines = File.ReadAllLines("DataPendaftarD3.txt");
                foreach (var cellArray in lines)
                {
                    if (cellArray.Contains(txtNISN.Text))
                    {
                        temp = cellArray.Remove(0);
                        continue;

                    }
                    newFile.Append(cellArray + "\r\n");

                }
                File.WriteAllText("DataPendaftarD3.txt", newFile.ToString());
                MessageBox.Show("Data Has been deleted successfully");
                Display();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            MenuAdmin obj = new MenuAdmin();
            obj.Show();
            this.Hide();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            RPTMaba obj = new RPTMaba();
            obj.Show();
            
        }
    }
}
