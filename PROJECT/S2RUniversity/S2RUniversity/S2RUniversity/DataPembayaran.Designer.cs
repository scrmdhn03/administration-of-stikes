﻿namespace S2RUniversity
{
    partial class DataPembayaran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.mcbSemester = new MetroFramework.Controls.MetroComboBox();
            this.mcbTahunAjaran = new MetroFramework.Controls.MetroComboBox();
            this.mcbJurusan = new MetroFramework.Controls.MetroComboBox();
            this.txtTanggalPembayaran = new MetroFramework.Controls.MetroDateTime();
            this.txtNIM = new MetroFramework.Controls.MetroTextBox();
            this.txtNamaLengkap = new MetroFramework.Controls.MetroTextBox();
            this.txtJumlahUang = new MetroFramework.Controls.MetroTextBox();
            this.mbSave = new MetroFramework.Controls.MetroButton();
            this.dgPembayaran = new MetroFramework.Controls.MetroGrid();
            this.ColNIM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNamaLengkap = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTanggalLahir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColJurusan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTahunAjaran = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColSemester = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColJenisKelamin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlamat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNoTelp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColJumlahUang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTanggalPembayaran = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColStatusBayar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.txtStatusBayar = new MetroFramework.Controls.MetroComboBox();
            this.cbJenisKelamin = new MetroFramework.Controls.MetroComboBox();
            this.txtAlamat = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.txtNoTelp = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.mdTanggalLahir = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.btnUpdate = new MetroFramework.Controls.MetroButton();
            this.btnMenuAdmin = new MetroFramework.Controls.MetroButton();
            this.btnReport = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgPembayaran)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(465, 73);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(179, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Data Pembayaran Mahasiswa";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(144, 148);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(34, 19);
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "NIM";
            this.metroLabel2.Click += new System.EventHandler(this.metroLabel2_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(144, 188);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(98, 19);
            this.metroLabel3.TabIndex = 2;
            this.metroLabel3.Text = "Nama Lengkap";
            this.metroLabel3.Click += new System.EventHandler(this.metroLabel3_Click);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(144, 272);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(52, 19);
            this.metroLabel4.TabIndex = 3;
            this.metroLabel4.Text = "Jurusan";
            this.metroLabel4.Click += new System.EventHandler(this.metroLabel4_Click);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(144, 319);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(84, 19);
            this.metroLabel5.TabIndex = 4;
            this.metroLabel5.Text = "Tahun Ajaran";
            this.metroLabel5.Click += new System.EventHandler(this.metroLabel5_Click);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(144, 375);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(63, 19);
            this.metroLabel6.TabIndex = 5;
            this.metroLabel6.Text = "Semester";
            this.metroLabel6.Click += new System.EventHandler(this.metroLabel6_Click);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(610, 282);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(85, 19);
            this.metroLabel7.TabIndex = 6;
            this.metroLabel7.Text = "Jumlah Uang";
            this.metroLabel7.Click += new System.EventHandler(this.metroLabel7_Click);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(610, 329);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(131, 19);
            this.metroLabel8.TabIndex = 7;
            this.metroLabel8.Text = "Tanggal Pembayaran";
            this.metroLabel8.Click += new System.EventHandler(this.metroLabel8_Click);
            // 
            // mcbSemester
            // 
            this.mcbSemester.FormattingEnabled = true;
            this.mcbSemester.ItemHeight = 23;
            this.mcbSemester.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.mcbSemester.Location = new System.Drawing.Point(311, 372);
            this.mcbSemester.Name = "mcbSemester";
            this.mcbSemester.Size = new System.Drawing.Size(205, 29);
            this.mcbSemester.TabIndex = 8;
            this.mcbSemester.UseSelectable = true;
            this.mcbSemester.SelectedIndexChanged += new System.EventHandler(this.mcbSemester_SelectedIndexChanged);
            // 
            // mcbTahunAjaran
            // 
            this.mcbTahunAjaran.FormattingEnabled = true;
            this.mcbTahunAjaran.ItemHeight = 23;
            this.mcbTahunAjaran.Items.AddRange(new object[] {
            "2015-2016",
            "2016-2017",
            "2017-2018",
            "2018-2019",
            "",
            ""});
            this.mcbTahunAjaran.Location = new System.Drawing.Point(311, 319);
            this.mcbTahunAjaran.Name = "mcbTahunAjaran";
            this.mcbTahunAjaran.Size = new System.Drawing.Size(205, 29);
            this.mcbTahunAjaran.TabIndex = 9;
            this.mcbTahunAjaran.UseSelectable = true;
            this.mcbTahunAjaran.SelectedIndexChanged += new System.EventHandler(this.mcbTahunAjaran_SelectedIndexChanged);
            // 
            // mcbJurusan
            // 
            this.mcbJurusan.FormattingEnabled = true;
            this.mcbJurusan.ItemHeight = 23;
            this.mcbJurusan.Items.AddRange(new object[] {
            "Gizi",
            "Keperawatan",
            "Kebidananan",
            "Kesehatan Lingkungan"});
            this.mcbJurusan.Location = new System.Drawing.Point(311, 272);
            this.mcbJurusan.Name = "mcbJurusan";
            this.mcbJurusan.Size = new System.Drawing.Size(205, 29);
            this.mcbJurusan.TabIndex = 10;
            this.mcbJurusan.UseSelectable = true;
            this.mcbJurusan.SelectedIndexChanged += new System.EventHandler(this.mcbJurusan_SelectedIndexChanged);
            // 
            // txtTanggalPembayaran
            // 
            this.txtTanggalPembayaran.Location = new System.Drawing.Point(767, 327);
            this.txtTanggalPembayaran.MinimumSize = new System.Drawing.Size(0, 29);
            this.txtTanggalPembayaran.Name = "txtTanggalPembayaran";
            this.txtTanggalPembayaran.Size = new System.Drawing.Size(179, 29);
            this.txtTanggalPembayaran.TabIndex = 11;
            this.txtTanggalPembayaran.ValueChanged += new System.EventHandler(this.mdTanggalPembayaran_ValueChanged);
            // 
            // txtNIM
            // 
            // 
            // 
            // 
            this.txtNIM.CustomButton.Image = null;
            this.txtNIM.CustomButton.Location = new System.Drawing.Point(183, 1);
            this.txtNIM.CustomButton.Name = "";
            this.txtNIM.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNIM.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNIM.CustomButton.TabIndex = 1;
            this.txtNIM.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNIM.CustomButton.UseSelectable = true;
            this.txtNIM.CustomButton.Visible = false;
            this.txtNIM.Lines = new string[0];
            this.txtNIM.Location = new System.Drawing.Point(311, 143);
            this.txtNIM.MaxLength = 32767;
            this.txtNIM.Name = "txtNIM";
            this.txtNIM.PasswordChar = '\0';
            this.txtNIM.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNIM.SelectedText = "";
            this.txtNIM.SelectionLength = 0;
            this.txtNIM.SelectionStart = 0;
            this.txtNIM.ShortcutsEnabled = true;
            this.txtNIM.Size = new System.Drawing.Size(205, 23);
            this.txtNIM.TabIndex = 12;
            this.txtNIM.UseSelectable = true;
            this.txtNIM.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNIM.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNIM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNIM_KeyDown);
            // 
            // txtNamaLengkap
            // 
            // 
            // 
            // 
            this.txtNamaLengkap.CustomButton.Image = null;
            this.txtNamaLengkap.CustomButton.Location = new System.Drawing.Point(183, 1);
            this.txtNamaLengkap.CustomButton.Name = "";
            this.txtNamaLengkap.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNamaLengkap.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNamaLengkap.CustomButton.TabIndex = 1;
            this.txtNamaLengkap.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNamaLengkap.CustomButton.UseSelectable = true;
            this.txtNamaLengkap.CustomButton.Visible = false;
            this.txtNamaLengkap.Lines = new string[0];
            this.txtNamaLengkap.Location = new System.Drawing.Point(311, 184);
            this.txtNamaLengkap.MaxLength = 32767;
            this.txtNamaLengkap.Name = "txtNamaLengkap";
            this.txtNamaLengkap.PasswordChar = '\0';
            this.txtNamaLengkap.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNamaLengkap.SelectedText = "";
            this.txtNamaLengkap.SelectionLength = 0;
            this.txtNamaLengkap.SelectionStart = 0;
            this.txtNamaLengkap.ShortcutsEnabled = true;
            this.txtNamaLengkap.Size = new System.Drawing.Size(205, 23);
            this.txtNamaLengkap.TabIndex = 13;
            this.txtNamaLengkap.UseSelectable = true;
            this.txtNamaLengkap.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNamaLengkap.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNamaLengkap.Click += new System.EventHandler(this.mbNamaLengkap_Click);
            // 
            // txtJumlahUang
            // 
            // 
            // 
            // 
            this.txtJumlahUang.CustomButton.Image = null;
            this.txtJumlahUang.CustomButton.Location = new System.Drawing.Point(156, 1);
            this.txtJumlahUang.CustomButton.Name = "";
            this.txtJumlahUang.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtJumlahUang.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtJumlahUang.CustomButton.TabIndex = 1;
            this.txtJumlahUang.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtJumlahUang.CustomButton.UseSelectable = true;
            this.txtJumlahUang.CustomButton.Visible = false;
            this.txtJumlahUang.Lines = new string[0];
            this.txtJumlahUang.Location = new System.Drawing.Point(767, 282);
            this.txtJumlahUang.MaxLength = 32767;
            this.txtJumlahUang.Name = "txtJumlahUang";
            this.txtJumlahUang.PasswordChar = '\0';
            this.txtJumlahUang.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtJumlahUang.SelectedText = "";
            this.txtJumlahUang.SelectionLength = 0;
            this.txtJumlahUang.SelectionStart = 0;
            this.txtJumlahUang.ShortcutsEnabled = true;
            this.txtJumlahUang.Size = new System.Drawing.Size(178, 23);
            this.txtJumlahUang.TabIndex = 14;
            this.txtJumlahUang.UseSelectable = true;
            this.txtJumlahUang.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtJumlahUang.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtJumlahUang.Click += new System.EventHandler(this.mcbJumlahUang_Click);
            // 
            // mbSave
            // 
            this.mbSave.Location = new System.Drawing.Point(925, 466);
            this.mbSave.Name = "mbSave";
            this.mbSave.Size = new System.Drawing.Size(91, 34);
            this.mbSave.TabIndex = 16;
            this.mbSave.Text = "SAVE";
            this.mbSave.UseSelectable = true;
            this.mbSave.Click += new System.EventHandler(this.mbSave_Click);
            // 
            // dgPembayaran
            // 
            this.dgPembayaran.AllowUserToResizeRows = false;
            this.dgPembayaran.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgPembayaran.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgPembayaran.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgPembayaran.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPembayaran.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgPembayaran.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPembayaran.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColNIM,
            this.ColNamaLengkap,
            this.CTanggalLahir,
            this.ColJurusan,
            this.ColTahunAjaran,
            this.ColSemester,
            this.ColJenisKelamin,
            this.ColAlamat,
            this.ColNoTelp,
            this.ColJumlahUang,
            this.ColTanggalPembayaran,
            this.ColStatusBayar});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPembayaran.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgPembayaran.EnableHeadersVisualStyles = false;
            this.dgPembayaran.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgPembayaran.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgPembayaran.Location = new System.Drawing.Point(201, 480);
            this.dgPembayaran.Name = "dgPembayaran";
            this.dgPembayaran.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPembayaran.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgPembayaran.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgPembayaran.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPembayaran.Size = new System.Drawing.Size(685, 87);
            this.dgPembayaran.TabIndex = 18;
            this.dgPembayaran.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGrid2_CellContentClick);
            // 
            // ColNIM
            // 
            this.ColNIM.HeaderText = "NIM";
            this.ColNIM.Name = "ColNIM";
            // 
            // ColNamaLengkap
            // 
            this.ColNamaLengkap.HeaderText = "Nama Lengkap";
            this.ColNamaLengkap.Name = "ColNamaLengkap";
            // 
            // CTanggalLahir
            // 
            this.CTanggalLahir.HeaderText = "Tanggal Lahir";
            this.CTanggalLahir.Name = "CTanggalLahir";
            // 
            // ColJurusan
            // 
            this.ColJurusan.HeaderText = "Jurusan";
            this.ColJurusan.Name = "ColJurusan";
            // 
            // ColTahunAjaran
            // 
            this.ColTahunAjaran.HeaderText = "Tahun Ajaran";
            this.ColTahunAjaran.Name = "ColTahunAjaran";
            // 
            // ColSemester
            // 
            this.ColSemester.HeaderText = "Semester ";
            this.ColSemester.Name = "ColSemester";
            // 
            // ColJenisKelamin
            // 
            this.ColJenisKelamin.HeaderText = "JenisvKelamin";
            this.ColJenisKelamin.Name = "ColJenisKelamin";
            // 
            // ColAlamat
            // 
            this.ColAlamat.HeaderText = "Alamat";
            this.ColAlamat.Name = "ColAlamat";
            // 
            // ColNoTelp
            // 
            this.ColNoTelp.HeaderText = "No Telp";
            this.ColNoTelp.Name = "ColNoTelp";
            // 
            // ColJumlahUang
            // 
            this.ColJumlahUang.HeaderText = "JumlahUang";
            this.ColJumlahUang.Name = "ColJumlahUang";
            // 
            // ColTanggalPembayaran
            // 
            this.ColTanggalPembayaran.HeaderText = "Tanggal Pembayaran";
            this.ColTanggalPembayaran.Name = "ColTanggalPembayaran";
            // 
            // ColStatusBayar
            // 
            this.ColStatusBayar.HeaderText = "Status Bayar";
            this.ColStatusBayar.Name = "ColStatusBayar";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(610, 382);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(80, 19);
            this.metroLabel9.TabIndex = 19;
            this.metroLabel9.Text = "Status Bayar";
            this.metroLabel9.Click += new System.EventHandler(this.metroLabel9_Click);
            // 
            // txtStatusBayar
            // 
            this.txtStatusBayar.FormattingEnabled = true;
            this.txtStatusBayar.ItemHeight = 23;
            this.txtStatusBayar.Items.AddRange(new object[] {
            "Lunas",
            "Belum Lunas"});
            this.txtStatusBayar.Location = new System.Drawing.Point(766, 377);
            this.txtStatusBayar.Name = "txtStatusBayar";
            this.txtStatusBayar.Size = new System.Drawing.Size(180, 29);
            this.txtStatusBayar.TabIndex = 20;
            this.txtStatusBayar.UseSelectable = true;
            this.txtStatusBayar.SelectedIndexChanged += new System.EventHandler(this.mcbStatusBayar_SelectedIndexChanged);
            // 
            // cbJenisKelamin
            // 
            this.cbJenisKelamin.FormattingEnabled = true;
            this.cbJenisKelamin.ItemHeight = 23;
            this.cbJenisKelamin.Items.AddRange(new object[] {
            "Perempuan",
            "Laki-Laki"});
            this.cbJenisKelamin.Location = new System.Drawing.Point(764, 148);
            this.cbJenisKelamin.Name = "cbJenisKelamin";
            this.cbJenisKelamin.Size = new System.Drawing.Size(181, 29);
            this.cbJenisKelamin.TabIndex = 45;
            this.cbJenisKelamin.UseSelectable = true;
            // 
            // txtAlamat
            // 
            // 
            // 
            // 
            this.txtAlamat.CustomButton.Image = null;
            this.txtAlamat.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtAlamat.CustomButton.Name = "";
            this.txtAlamat.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtAlamat.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAlamat.CustomButton.TabIndex = 1;
            this.txtAlamat.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAlamat.CustomButton.UseSelectable = true;
            this.txtAlamat.CustomButton.Visible = false;
            this.txtAlamat.Lines = new string[0];
            this.txtAlamat.Location = new System.Drawing.Point(764, 192);
            this.txtAlamat.MaxLength = 32767;
            this.txtAlamat.Name = "txtAlamat";
            this.txtAlamat.PasswordChar = '\0';
            this.txtAlamat.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAlamat.SelectedText = "";
            this.txtAlamat.SelectionLength = 0;
            this.txtAlamat.SelectionStart = 0;
            this.txtAlamat.ShortcutsEnabled = true;
            this.txtAlamat.Size = new System.Drawing.Size(181, 23);
            this.txtAlamat.TabIndex = 44;
            this.txtAlamat.UseSelectable = true;
            this.txtAlamat.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAlamat.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(609, 188);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(51, 19);
            this.metroLabel10.TabIndex = 43;
            this.metroLabel10.Text = "Alamat";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(609, 143);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(86, 19);
            this.metroLabel11.TabIndex = 42;
            this.metroLabel11.Text = "Jenis Kelamin";
            // 
            // txtNoTelp
            // 
            // 
            // 
            // 
            this.txtNoTelp.CustomButton.Image = null;
            this.txtNoTelp.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.txtNoTelp.CustomButton.Name = "";
            this.txtNoTelp.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNoTelp.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNoTelp.CustomButton.TabIndex = 1;
            this.txtNoTelp.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNoTelp.CustomButton.UseSelectable = true;
            this.txtNoTelp.CustomButton.Visible = false;
            this.txtNoTelp.Lines = new string[0];
            this.txtNoTelp.Location = new System.Drawing.Point(768, 233);
            this.txtNoTelp.MaxLength = 32767;
            this.txtNoTelp.Name = "txtNoTelp";
            this.txtNoTelp.PasswordChar = '\0';
            this.txtNoTelp.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNoTelp.SelectedText = "";
            this.txtNoTelp.SelectionLength = 0;
            this.txtNoTelp.SelectionStart = 0;
            this.txtNoTelp.ShortcutsEnabled = true;
            this.txtNoTelp.Size = new System.Drawing.Size(177, 23);
            this.txtNoTelp.TabIndex = 47;
            this.txtNoTelp.UseSelectable = true;
            this.txtNoTelp.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNoTelp.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(609, 236);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(57, 19);
            this.metroLabel12.TabIndex = 46;
            this.metroLabel12.Text = "No. Telp";
            // 
            // mdTanggalLahir
            // 
            this.mdTanggalLahir.Location = new System.Drawing.Point(312, 226);
            this.mdTanggalLahir.MinimumSize = new System.Drawing.Size(0, 29);
            this.mdTanggalLahir.Name = "mdTanggalLahir";
            this.mdTanggalLahir.Size = new System.Drawing.Size(178, 29);
            this.mdTanggalLahir.TabIndex = 49;
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(144, 226);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(86, 19);
            this.metroLabel13.TabIndex = 48;
            this.metroLabel13.Text = "Tanggal Lahir";
            this.metroLabel13.Click += new System.EventHandler(this.metroLabel13_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(925, 424);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(91, 36);
            this.btnUpdate.TabIndex = 50;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseSelectable = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnMenuAdmin
            // 
            this.btnMenuAdmin.Location = new System.Drawing.Point(925, 519);
            this.btnMenuAdmin.Name = "btnMenuAdmin";
            this.btnMenuAdmin.Size = new System.Drawing.Size(91, 34);
            this.btnMenuAdmin.TabIndex = 51;
            this.btnMenuAdmin.Text = "Menu Admin";
            this.btnMenuAdmin.UseSelectable = true;
            this.btnMenuAdmin.Click += new System.EventHandler(this.btnMenuAdmin_Click);
            // 
            // btnReport
            // 
            this.btnReport.Location = new System.Drawing.Point(925, 566);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(91, 34);
            this.btnReport.TabIndex = 52;
            this.btnReport.Text = "Report";
            this.btnReport.UseSelectable = true;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // DataPembayaran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1078, 623);
            this.Controls.Add(this.btnReport);
            this.Controls.Add(this.btnMenuAdmin);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.mdTanggalLahir);
            this.Controls.Add(this.metroLabel13);
            this.Controls.Add(this.txtNoTelp);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.cbJenisKelamin);
            this.Controls.Add(this.txtAlamat);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.txtStatusBayar);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.dgPembayaran);
            this.Controls.Add(this.mbSave);
            this.Controls.Add(this.txtJumlahUang);
            this.Controls.Add(this.txtNamaLengkap);
            this.Controls.Add(this.txtNIM);
            this.Controls.Add(this.txtTanggalPembayaran);
            this.Controls.Add(this.mcbJurusan);
            this.Controls.Add(this.mcbTahunAjaran);
            this.Controls.Add(this.mcbSemester);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Name = "DataPembayaran";
            this.Text = "DataPembayaran";
            this.Load += new System.EventHandler(this.DataPembayaran_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgPembayaran)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroComboBox mcbSemester;
        private MetroFramework.Controls.MetroComboBox mcbTahunAjaran;
        private MetroFramework.Controls.MetroComboBox mcbJurusan;
        private MetroFramework.Controls.MetroDateTime txtTanggalPembayaran;
        private MetroFramework.Controls.MetroTextBox txtNIM;
        private MetroFramework.Controls.MetroTextBox txtNamaLengkap;
        private MetroFramework.Controls.MetroTextBox txtJumlahUang;
        private MetroFramework.Controls.MetroButton mbSave;
        private MetroFramework.Controls.MetroGrid dgPembayaran;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroComboBox txtStatusBayar;
        private MetroFramework.Controls.MetroComboBox cbJenisKelamin;
        private MetroFramework.Controls.MetroTextBox txtAlamat;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroTextBox txtNoTelp;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroDateTime mdTanggalLahir;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroButton btnUpdate;
        private MetroFramework.Controls.MetroButton btnMenuAdmin;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTanggalLahir;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColStatusBayar;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTanggalPembayaran;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColJumlahUang;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNoTelp;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlamat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColJenisKelamin;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColSemester;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTahunAjaran;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColJurusan;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNamaLengkap;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNIM;
        private MetroFramework.Controls.MetroButton btnReport;
    }
}