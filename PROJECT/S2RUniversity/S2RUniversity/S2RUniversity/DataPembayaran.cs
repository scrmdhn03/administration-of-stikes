﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace S2RUniversity
{
    public partial class DataPembayaran : MetroForm
    {
        public DataPembayaran()
        {
            InitializeComponent();
        }


        private void metroGrid2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        public void Display()
        {
            dgPembayaran.Rows.Clear();
            var lines = File.ReadAllLines("DataPembayaran2.txt");
            if (lines.Count() > 0)
            {
                foreach (var cellArray in lines)
                {
                    var cell = cellArray.Split('#');
                    if (cell.Length == dgPembayaran.Columns.Count)

                        dgPembayaran.Rows.Add(cell);

                }
            }
        }
        public static string NamaLengkap, TanggalLahir, Jurusan, TahunAjaran, Semester, JenisKelamin, Alamat, NoTelp, JumlahUang, TanggalPembayaran, StatusPembayaran;
        private void mbSave_Click(object sender, EventArgs e)
        {
            string Pembayaran;
            FileStream fs = new FileStream("DataPembayaran2.txt", FileMode.Append, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);

            Pembayaran = txtNIM.Text + "#" + txtNamaLengkap.Text + "#" + mdTanggalLahir.Text + "#" + mcbJurusan.Text + "#" + mcbTahunAjaran.Text + "#" +
                mcbSemester.Text + "#" + cbJenisKelamin.Text + "#" + txtAlamat.Text + "#" + txtNoTelp.Text + "#" + txtJumlahUang.Text + "#" + txtTanggalPembayaran.Text + "#"
                + txtStatusBayar.Text;
            sw.WriteLine(Pembayaran);
            sw.Flush();
            sw.Close();
            fs.Close();


            MessageBox.Show("Berhasil Daftar", "information", MessageBoxButtons.OK, MessageBoxIcon.Information); //buat yang are you sure gtgt kalo messageboxbuttons
            NamaLengkap = txtNamaLengkap.Text;
          
            Jurusan = mcbJurusan.Text;
            TahunAjaran = mcbTahunAjaran.Text;
            Semester = mcbSemester.Text;
           
            Alamat = txtAlamat.Text;
            NoTelp = txtNoTelp.Text;
            JumlahUang = txtJumlahUang.Text;
            TanggalPembayaran = txtTanggalPembayaran.Text;
            StatusPembayaran = txtStatusBayar.Text;

            txtNamaLengkap.Clear();
            txtAlamat.Clear();
            txtNoTelp.Clear();
            txtJumlahUang.Clear();
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void metroLabel9_Click(object sender, EventArgs e)
        {

        }

        private void mcbStatusBayar_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void mcbJumlahUang_Click(object sender, EventArgs e)
        {

        }

        private void mbNamaLengkap_Click(object sender, EventArgs e)
        {

        }

        private void mdTanggalPembayaran_ValueChanged(object sender, EventArgs e)
        {

        }

        private void mcbJurusan_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void mcbTahunAjaran_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void mcbSemester_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void metroLabel8_Click(object sender, EventArgs e)
        {

        }

        private void btnMenuAdmin_Click(object sender, EventArgs e)
        {
            MenuAdmin obj = new MenuAdmin();
            obj.Show();
            this.Hide();
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            RptPembayaran obj = new RptPembayaran();
            obj.Show();
        }

        private void metroLabel7_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel6_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel5_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel4_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel3_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel2_Click(object sender, EventArgs e)
        {

        }

        private void DataPembayaran_Load(object sender, EventArgs e)
        {
            Display();
        }

        private void metroLabel13_Click(object sender, EventArgs e)
        {

        }

        private void txtNIM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {// kalo di enter jadi langsung masuk ke yg baru di cari gak kosong dl
                try
                {
                    dgPembayaran.Rows.Clear();
                    var Lines = File.ReadAllLines("DataPembayaran2.txt");
                    if (Lines.Count() > 0)
                    {
                        foreach (var cellArray in Lines)
                        {
                            var cell = cellArray.Split('#');

                            if (cell.Contains(txtNIM.Text))
                            {
                                if (cell.Length == dgPembayaran.Columns.Count)
                                    dgPembayaran.Rows.Add(cell);
                                    txtNamaLengkap.Text = cell[1];
                                    mdTanggalLahir.Text = cell[2];
                                    mcbJurusan.Text = cell[3];
                                    mcbTahunAjaran.Text = cell[4];
                                    mcbSemester.Text = cell[5];
                                    cbJenisKelamin.Text = cell[6];
                                    txtAlamat.Text = cell[7];
                                    txtNoTelp.Text = cell[8];
                                    txtJumlahUang.Text = cell[9];
                                    txtTanggalPembayaran.Text = cell[10];
                                    txtStatusBayar.Text = cell[11];
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> newfile = new List<string>();
                string temp = "";
                dgPembayaran.Rows.Clear();
                var lines = File.ReadAllLines("DataPembayaran2.txt");

                foreach (var cellArray in lines)
                {
                    var data = cellArray.Split('#');
                    if (data[0].Equals(txtNIM.Text))
                    {
                        data[1] = txtNamaLengkap.Text.Trim(); //jika yg di ubah banyak ini nya banyak
                        data[2] = mdTanggalLahir.Text;
                        data[3] = mcbJurusan.Text;
                        data[4] = mcbTahunAjaran.Text;
                        data[5] = mcbSemester.Text;
                        data[6] = cbJenisKelamin.Text;
                        data[7] = txtAlamat.Text;
                        data[8] = txtNoTelp.Text;
                        data[9] = txtJumlahUang.Text;
                        data[10] = txtTanggalPembayaran.Text;
                        data[11] = txtStatusBayar.Text;
                    }
                    newfile.Add(string.Join("#", data));
                }
                File.WriteAllLines("DataPembayaran2.txt", newfile);

                MessageBox.Show("Data has been updated");
                Display();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
