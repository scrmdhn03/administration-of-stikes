﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace S2RUniversity
{
    public partial class InputAdmin : Form
    {
        public InputAdmin()
        {
            InitializeComponent();
        }

        private void txtIDAdmin_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dgvAdmin.Rows.Clear();
                var lines = File.ReadAllLines("InputAdmin.txt");
                if (lines.Count() > 0)
                {
                    foreach (var data in lines)
                    {
                        var cell = data.Split('#');
                        if (cell.Contains(txtUsername.Text))
                        {
                            if (cell.Length == dgvAdmin.Columns.Count)
                                dgvAdmin.Rows.Add(cell);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(),
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void Display()
        {
            dgvAdmin.Rows.Clear();
            var lines = File.ReadAllLines("InputAdmin.txt");
            if (lines.Count() > 0)
            {
                foreach (var cellArray in lines)
                {
                    var cell = cellArray.Split('#');
                    if (cell.Length == dgvAdmin.Columns.Count)

                        dgvAdmin.Rows.Add(cell);

                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Regex rgxusrp = new Regex("^[a-zA-Z0-9 ]+$");
            var dbIA = File.ReadAllLines("InputAdmin.txt");
            //string[] ary = new string[2];
            foreach (string data in dbIA)
            {
                var ary = data.Split('#');
                if(ary[0].Equals(txtUsername.Text) && ary[1].Equals(txtPassword.Text))
                {
                    MessageBox.Show("Username or Password already exist.", "Error!",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }


            if (txtPassword.Text.Length == 0 || txtUsername.Text.Length == 0)
            {
                MessageBox.Show("Username or Password Cannot Be null.", "Error!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!rgxusrp.IsMatch(txtUsername.Text) || !rgxusrp.IsMatch(txtPassword.Text))
            {
                MessageBox.Show("Username or Password not valid.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    FileStream fs = new FileStream("InputAdmin.txt", FileMode.Append,
                        FileAccess.Write, FileShare.ReadWrite);
                    StreamWriter sw = new StreamWriter(fs);

                    sw.WriteLine(txtUsername.Text.Trim() + "#" + txtPassword.Text.Trim());
                    MessageBox.Show("Saving data successful.");
                    sw.Flush();
                    sw.Close();
                    fs.Close();
                    Display(); //Ini teh kenapa error :(
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(),
                        "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);


                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> newFile = new List<string>();
                //string temp = "";
                dgvAdmin.Rows.Clear();
                var lines = File.ReadAllLines("InputAdmin.txt");

                foreach (var cellArray in lines)
                {
                    var data = cellArray.Split('#');
                    if (data[0].Equals(txtUsername.Text))
                    {
                        data[1] = txtPassword.Text.Trim();

                    }
                    newFile.Add(string.Join("#", data));

                }
                
                File.WriteAllLines("InputAdmin.txt", newFile);

                MessageBox.Show("Data has been updated");
                Display();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder newFile = new StringBuilder();
                string temp = "";
                dgvAdmin.Rows.Clear();
                var lines = File.ReadAllLines("InputAdmin.txt");

                foreach (var cellArray in lines)
                {

                    if (cellArray.Contains(txtUsername.Text))
                    {
                        temp = cellArray.Remove(0);
                        continue;
                    }
                    newFile.Append(cellArray + "\r\n");

                }
                File.WriteAllText("InputAdmin.txt", newFile.ToString());
                MessageBox.Show("Data has been deleted successfully");
                Display();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvAdmin_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}

