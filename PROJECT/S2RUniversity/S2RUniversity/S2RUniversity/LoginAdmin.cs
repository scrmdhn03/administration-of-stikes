﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.IO;

namespace S2RUniversity
{
    public partial class LoginAdmin : MetroForm
    {
        public LoginAdmin()
        {
            InitializeComponent();
        }

        private void txtLogin_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnLogin1_Click(object sender, EventArgs e)
        {
            if ((txtUsername.Text.Length == 0) || (txtPassword.Text.Length == 0))
            {
                MessageBox.Show("Username and Password not null!");
            }
            else
            {
                FileStream fs = new FileStream("InputAdmin.txt", FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(fs);
                var file = File.ReadAllLines("InputAdmin.txt");
                dynamic ArrLog = "";
                foreach (string dataLogin in file)
                {
                    ArrLog = dataLogin.Split('#');
                    if ((ArrLog[0].Equals(txtUsername.Text)) && (ArrLog[1].Equals(txtPassword.Text)))
                    {
                        break;
                    }
                }
                if ((ArrLog[0].Equals(txtUsername.Text)) && (ArrLog[1].Equals(txtPassword.Text)))
                {
                    MenuAdmin frm = new MenuAdmin();
                    frm.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Username or Password invalid !");
                }
            }
        }
    }
}
