﻿namespace S2RUniversity
{
    partial class MenuAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPembayaran = new MetroFramework.Controls.MetroButton();
            this.btnDaftarMaba = new MetroFramework.Controls.MetroButton();
            this.btnDaftarMahasiswa = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // btnPembayaran
            // 
            this.btnPembayaran.Location = new System.Drawing.Point(179, 67);
            this.btnPembayaran.Name = "btnPembayaran";
            this.btnPembayaran.Size = new System.Drawing.Size(160, 37);
            this.btnPembayaran.TabIndex = 3;
            this.btnPembayaran.Text = "Pembayaran";
            this.btnPembayaran.UseSelectable = true;
            this.btnPembayaran.Click += new System.EventHandler(this.btnPembayaran_Click_2);
            // 
            // btnDaftarMaba
            // 
            this.btnDaftarMaba.Location = new System.Drawing.Point(180, 125);
            this.btnDaftarMaba.Name = "btnDaftarMaba";
            this.btnDaftarMaba.Size = new System.Drawing.Size(158, 45);
            this.btnDaftarMaba.TabIndex = 4;
            this.btnDaftarMaba.Text = "Daftar Mahasiswa Baru";
            this.btnDaftarMaba.UseSelectable = true;
            this.btnDaftarMaba.Click += new System.EventHandler(this.btnDaftarMaba_Click_1);
            // 
            // btnDaftarMahasiswa
            // 
            this.btnDaftarMahasiswa.Location = new System.Drawing.Point(179, 190);
            this.btnDaftarMahasiswa.Name = "btnDaftarMahasiswa";
            this.btnDaftarMahasiswa.Size = new System.Drawing.Size(159, 43);
            this.btnDaftarMahasiswa.TabIndex = 5;
            this.btnDaftarMahasiswa.Text = "Daftar Mahasiswa";
            this.btnDaftarMahasiswa.UseSelectable = true;
            this.btnDaftarMahasiswa.Click += new System.EventHandler(this.btnDaftarMahasiswa_Click_1);
            // 
            // MenuAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 297);
            this.Controls.Add(this.btnDaftarMahasiswa);
            this.Controls.Add(this.btnDaftarMaba);
            this.Controls.Add(this.btnPembayaran);
            this.Name = "MenuAdmin";
            this.Text = "MenuMain";
            this.Load += new System.EventHandler(this.MenuMain_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroButton btnDaftarMahasiswa;
        private MetroFramework.Controls.MetroButton btnDaftarMaba;
        private MetroFramework.Controls.MetroButton btnPembayaran;
    }
}