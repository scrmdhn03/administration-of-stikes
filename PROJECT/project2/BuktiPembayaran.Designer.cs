﻿namespace project2
{
    partial class BuktiPembayaran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.txtjumlah = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.txttotalterbilang = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtJurusan = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtNama = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtTanggal = new MetroFramework.Controls.MetroTextBox();
            this.btnPrint = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel8.Location = new System.Drawing.Point(246, 77);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(122, 25);
            this.metroLabel8.TabIndex = 35;
            this.metroLabel8.Text = "Pembayaran ";
            // 
            // txtjumlah
            // 
            // 
            // 
            // 
            this.txtjumlah.CustomButton.Image = null;
            this.txtjumlah.CustomButton.Location = new System.Drawing.Point(348, 1);
            this.txtjumlah.CustomButton.Name = "";
            this.txtjumlah.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtjumlah.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtjumlah.CustomButton.TabIndex = 1;
            this.txtjumlah.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtjumlah.CustomButton.UseSelectable = true;
            this.txtjumlah.CustomButton.Visible = false;
            this.txtjumlah.Lines = new string[0];
            this.txtjumlah.Location = new System.Drawing.Point(271, 255);
            this.txtjumlah.MaxLength = 32767;
            this.txtjumlah.Name = "txtjumlah";
            this.txtjumlah.PasswordChar = '\0';
            this.txtjumlah.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtjumlah.SelectedText = "";
            this.txtjumlah.SelectionLength = 0;
            this.txtjumlah.SelectionStart = 0;
            this.txtjumlah.ShortcutsEnabled = true;
            this.txtjumlah.Size = new System.Drawing.Size(370, 23);
            this.txtjumlah.TabIndex = 33;
            this.txtjumlah.UseSelectable = true;
            this.txtjumlah.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtjumlah.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(79, 255);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(141, 19);
            this.metroLabel7.TabIndex = 32;
            this.metroLabel7.Text = "Jumlah                      :";
            // 
            // txttotalterbilang
            // 
            // 
            // 
            // 
            this.txttotalterbilang.CustomButton.Image = null;
            this.txttotalterbilang.CustomButton.Location = new System.Drawing.Point(348, 1);
            this.txttotalterbilang.CustomButton.Name = "";
            this.txttotalterbilang.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txttotalterbilang.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txttotalterbilang.CustomButton.TabIndex = 1;
            this.txttotalterbilang.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txttotalterbilang.CustomButton.UseSelectable = true;
            this.txttotalterbilang.CustomButton.Visible = false;
            this.txttotalterbilang.Lines = new string[0];
            this.txttotalterbilang.Location = new System.Drawing.Point(271, 216);
            this.txttotalterbilang.MaxLength = 32767;
            this.txttotalterbilang.Name = "txttotalterbilang";
            this.txttotalterbilang.PasswordChar = '\0';
            this.txttotalterbilang.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txttotalterbilang.SelectedText = "";
            this.txttotalterbilang.SelectionLength = 0;
            this.txttotalterbilang.SelectionStart = 0;
            this.txttotalterbilang.ShortcutsEnabled = true;
            this.txttotalterbilang.Size = new System.Drawing.Size(370, 23);
            this.txttotalterbilang.TabIndex = 31;
            this.txttotalterbilang.UseSelectable = true;
            this.txttotalterbilang.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txttotalterbilang.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(81, 220);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(140, 19);
            this.metroLabel6.TabIndex = 30;
            this.metroLabel6.Text = "Total Terbilang           :";
            // 
            // txtJurusan
            // 
            // 
            // 
            // 
            this.txtJurusan.CustomButton.Image = null;
            this.txtJurusan.CustomButton.Location = new System.Drawing.Point(348, 1);
            this.txtJurusan.CustomButton.Name = "";
            this.txtJurusan.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtJurusan.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtJurusan.CustomButton.TabIndex = 1;
            this.txtJurusan.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtJurusan.CustomButton.UseSelectable = true;
            this.txtJurusan.CustomButton.Visible = false;
            this.txtJurusan.Lines = new string[0];
            this.txtJurusan.Location = new System.Drawing.Point(271, 175);
            this.txtJurusan.MaxLength = 32767;
            this.txtJurusan.Name = "txtJurusan";
            this.txtJurusan.PasswordChar = '\0';
            this.txtJurusan.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtJurusan.SelectedText = "";
            this.txtJurusan.SelectionLength = 0;
            this.txtJurusan.SelectionStart = 0;
            this.txtJurusan.ShortcutsEnabled = true;
            this.txtJurusan.Size = new System.Drawing.Size(370, 23);
            this.txtJurusan.TabIndex = 24;
            this.txtJurusan.UseSelectable = true;
            this.txtJurusan.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtJurusan.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(81, 175);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(147, 19);
            this.metroLabel2.TabIndex = 23;
            this.metroLabel2.Text = "Jurusan                       :";
            // 
            // txtNama
            // 
            // 
            // 
            // 
            this.txtNama.CustomButton.Image = null;
            this.txtNama.CustomButton.Location = new System.Drawing.Point(348, 1);
            this.txtNama.CustomButton.Name = "";
            this.txtNama.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNama.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNama.CustomButton.TabIndex = 1;
            this.txtNama.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNama.CustomButton.UseSelectable = true;
            this.txtNama.CustomButton.Visible = false;
            this.txtNama.Lines = new string[0];
            this.txtNama.Location = new System.Drawing.Point(271, 134);
            this.txtNama.MaxLength = 32767;
            this.txtNama.Name = "txtNama";
            this.txtNama.PasswordChar = '\0';
            this.txtNama.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNama.SelectedText = "";
            this.txtNama.SelectionLength = 0;
            this.txtNama.SelectionStart = 0;
            this.txtNama.ShortcutsEnabled = true;
            this.txtNama.Size = new System.Drawing.Size(370, 23);
            this.txtNama.TabIndex = 22;
            this.txtNama.UseSelectable = true;
            this.txtNama.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNama.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(81, 138);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(144, 19);
            this.metroLabel1.TabIndex = 21;
            this.metroLabel1.Text = "Nama                        :";
            // 
            // txtTanggal
            // 
            // 
            // 
            // 
            this.txtTanggal.CustomButton.Image = null;
            this.txtTanggal.CustomButton.Location = new System.Drawing.Point(103, 1);
            this.txtTanggal.CustomButton.Name = "";
            this.txtTanggal.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtTanggal.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtTanggal.CustomButton.TabIndex = 1;
            this.txtTanggal.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtTanggal.CustomButton.UseSelectable = true;
            this.txtTanggal.CustomButton.Visible = false;
            this.txtTanggal.Lines = new string[0];
            this.txtTanggal.Location = new System.Drawing.Point(516, 329);
            this.txtTanggal.MaxLength = 32767;
            this.txtTanggal.Name = "txtTanggal";
            this.txtTanggal.PasswordChar = '\0';
            this.txtTanggal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTanggal.SelectedText = "";
            this.txtTanggal.SelectionLength = 0;
            this.txtTanggal.SelectionStart = 0;
            this.txtTanggal.ShortcutsEnabled = true;
            this.txtTanggal.Size = new System.Drawing.Size(125, 23);
            this.txtTanggal.TabIndex = 36;
            this.txtTanggal.UseSelectable = true;
            this.txtTanggal.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtTanggal.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnPrint
            // 
            this.btnPrint.BackgroundImage = global::project2.Properties.Resources.Print;
            this.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPrint.Location = new System.Drawing.Point(599, 29);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(80, 73);
            this.btnPrint.TabIndex = 37;
            this.btnPrint.UseSelectable = true;
            // 
            // metroButton1
            // 
            this.metroButton1.BackgroundImage = global::project2.Properties.Resources.Home_512;
            this.metroButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.metroButton1.Location = new System.Drawing.Point(94, 305);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(70, 62);
            this.metroButton1.TabIndex = 38;
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // BuktiPembayaran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 390);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.txtTanggal);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.txtjumlah);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.txttotalterbilang);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.txtJurusan);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.txtNama);
            this.Controls.Add(this.metroLabel1);
            this.Name = "BuktiPembayaran";
            this.Text = "BuktiPembayaran";
            this.Load += new System.EventHandler(this.BuktiPembayaran_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox txtjumlah;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox txttotalterbilang;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtJurusan;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtNama;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtTanggal;
        private MetroFramework.Controls.MetroButton btnPrint;
        private MetroFramework.Controls.MetroButton metroButton1;
    }
}