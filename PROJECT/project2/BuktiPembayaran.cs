﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.IO;

namespace project2
{
    public partial class BuktiPembayaran : MetroForm
    {
        public BuktiPembayaran()
        {
            InitializeComponent();
        }

        private void BuktiPembayaran_Load(object sender, EventArgs e)
        {
           
            txtNama.Text = PembayaranPendaftaran.Nama;
            txtJurusan.Text = PembayaranPendaftaran.Jurusan;
            txttotalterbilang.Text = PembayaranPendaftaran.Terbilang;
            txtjumlah.Text = PembayaranPendaftaran.Jumlah;
            txtTanggal.Text = PembayaranPendaftaran.Tanggal;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            MenuKurikulum obj = new MenuKurikulum();
            obj.Show();
            this.Hide();
        }
    }
}
