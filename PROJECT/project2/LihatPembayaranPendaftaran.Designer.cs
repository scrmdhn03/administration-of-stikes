﻿namespace project2
{
    partial class LihatPembayaranPendaftaran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtNama = new MetroFramework.Controls.MetroTextBox();
            this.txtJurusan = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtTanggal = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtjumlah = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.dgDataPembayaran = new MetroFramework.Controls.MetroGrid();
            this.CNISN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CJurusan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTanggalBayar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUangPendaftaran = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUangPangkal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CTerbilang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CJumlah = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.txtNISN = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.btnDelete = new MetroFramework.Controls.MetroButton();
            this.btnupdate = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgDataPembayaran)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(57, 216);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(92, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Nama           :";
            // 
            // txtNama
            // 
            // 
            // 
            // 
            this.txtNama.CustomButton.Image = null;
            this.txtNama.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtNama.CustomButton.Name = "";
            this.txtNama.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNama.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNama.CustomButton.TabIndex = 1;
            this.txtNama.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNama.CustomButton.UseSelectable = true;
            this.txtNama.CustomButton.Visible = false;
            this.txtNama.Lines = new string[0];
            this.txtNama.Location = new System.Drawing.Point(166, 216);
            this.txtNama.MaxLength = 32767;
            this.txtNama.Name = "txtNama";
            this.txtNama.PasswordChar = '\0';
            this.txtNama.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNama.SelectedText = "";
            this.txtNama.SelectionLength = 0;
            this.txtNama.SelectionStart = 0;
            this.txtNama.ShortcutsEnabled = true;
            this.txtNama.Size = new System.Drawing.Size(181, 23);
            this.txtNama.TabIndex = 1;
            this.txtNama.UseSelectable = true;
            this.txtNama.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNama.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNama.Click += new System.EventHandler(this.txtNama_Click);
            this.txtNama.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNama_KeyDown);
            // 
            // txtJurusan
            // 
            // 
            // 
            // 
            this.txtJurusan.CustomButton.Image = null;
            this.txtJurusan.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtJurusan.CustomButton.Name = "";
            this.txtJurusan.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtJurusan.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtJurusan.CustomButton.TabIndex = 1;
            this.txtJurusan.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtJurusan.CustomButton.UseSelectable = true;
            this.txtJurusan.CustomButton.Visible = false;
            this.txtJurusan.Lines = new string[0];
            this.txtJurusan.Location = new System.Drawing.Point(166, 273);
            this.txtJurusan.MaxLength = 32767;
            this.txtJurusan.Name = "txtJurusan";
            this.txtJurusan.PasswordChar = '\0';
            this.txtJurusan.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtJurusan.SelectedText = "";
            this.txtJurusan.SelectionLength = 0;
            this.txtJurusan.SelectionStart = 0;
            this.txtJurusan.ShortcutsEnabled = true;
            this.txtJurusan.Size = new System.Drawing.Size(181, 23);
            this.txtJurusan.TabIndex = 3;
            this.txtJurusan.UseSelectable = true;
            this.txtJurusan.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtJurusan.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtJurusan.Click += new System.EventHandler(this.metroTextBox2_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(57, 273);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(83, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Jurusan       :";
            this.metroLabel2.Click += new System.EventHandler(this.metroLabel2_Click);
            // 
            // txtTanggal
            // 
            // 
            // 
            // 
            this.txtTanggal.CustomButton.Image = null;
            this.txtTanggal.CustomButton.Location = new System.Drawing.Point(163, 1);
            this.txtTanggal.CustomButton.Name = "";
            this.txtTanggal.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtTanggal.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtTanggal.CustomButton.TabIndex = 1;
            this.txtTanggal.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtTanggal.CustomButton.UseSelectable = true;
            this.txtTanggal.CustomButton.Visible = false;
            this.txtTanggal.Lines = new string[0];
            this.txtTanggal.Location = new System.Drawing.Point(493, 182);
            this.txtTanggal.MaxLength = 32767;
            this.txtTanggal.Name = "txtTanggal";
            this.txtTanggal.PasswordChar = '\0';
            this.txtTanggal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTanggal.SelectedText = "";
            this.txtTanggal.SelectionLength = 0;
            this.txtTanggal.SelectionStart = 0;
            this.txtTanggal.ShortcutsEnabled = true;
            this.txtTanggal.Size = new System.Drawing.Size(185, 23);
            this.txtTanggal.TabIndex = 5;
            this.txtTanggal.UseSelectable = true;
            this.txtTanggal.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtTanggal.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(384, 186);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(98, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Tanggal Bayar :";
            // 
            // txtjumlah
            // 
            // 
            // 
            // 
            this.txtjumlah.CustomButton.Image = null;
            this.txtjumlah.CustomButton.Location = new System.Drawing.Point(163, 1);
            this.txtjumlah.CustomButton.Name = "";
            this.txtjumlah.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtjumlah.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtjumlah.CustomButton.TabIndex = 1;
            this.txtjumlah.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtjumlah.CustomButton.UseSelectable = true;
            this.txtjumlah.CustomButton.Visible = false;
            this.txtjumlah.Lines = new string[0];
            this.txtjumlah.Location = new System.Drawing.Point(493, 235);
            this.txtjumlah.MaxLength = 32767;
            this.txtjumlah.Name = "txtjumlah";
            this.txtjumlah.PasswordChar = '\0';
            this.txtjumlah.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtjumlah.SelectedText = "";
            this.txtjumlah.SelectionLength = 0;
            this.txtjumlah.SelectionStart = 0;
            this.txtjumlah.ShortcutsEnabled = true;
            this.txtjumlah.Size = new System.Drawing.Size(185, 23);
            this.txtjumlah.TabIndex = 7;
            this.txtjumlah.UseSelectable = true;
            this.txtjumlah.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtjumlah.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(384, 235);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(89, 19);
            this.metroLabel4.TabIndex = 6;
            this.metroLabel4.Text = "Jumlah         :";
            // 
            // dgDataPembayaran
            // 
            this.dgDataPembayaran.AllowUserToResizeRows = false;
            this.dgDataPembayaran.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgDataPembayaran.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgDataPembayaran.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgDataPembayaran.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDataPembayaran.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgDataPembayaran.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDataPembayaran.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CNISN,
            this.CNama,
            this.CJurusan,
            this.CTanggalBayar,
            this.CUangPendaftaran,
            this.CUangPangkal,
            this.CTerbilang,
            this.CJumlah});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDataPembayaran.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgDataPembayaran.EnableHeadersVisualStyles = false;
            this.dgDataPembayaran.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgDataPembayaran.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgDataPembayaran.Location = new System.Drawing.Point(54, 342);
            this.dgDataPembayaran.Name = "dgDataPembayaran";
            this.dgDataPembayaran.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDataPembayaran.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgDataPembayaran.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgDataPembayaran.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDataPembayaran.Size = new System.Drawing.Size(547, 119);
            this.dgDataPembayaran.TabIndex = 8;
            this.dgDataPembayaran.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDataPembayaran_CellContentClick);
            // 
            // CNISN
            // 
            this.CNISN.HeaderText = "NISN";
            this.CNISN.Name = "CNISN";
            // 
            // CNama
            // 
            this.CNama.HeaderText = "Nama";
            this.CNama.Name = "CNama";
            // 
            // CJurusan
            // 
            this.CJurusan.HeaderText = "Jurusan";
            this.CJurusan.Name = "CJurusan";
            // 
            // CTanggalBayar
            // 
            this.CTanggalBayar.HeaderText = "Tanggal Bayar";
            this.CTanggalBayar.Name = "CTanggalBayar";
            // 
            // CUangPendaftaran
            // 
            this.CUangPendaftaran.HeaderText = "Uang Pendaftaran";
            this.CUangPendaftaran.Name = "CUangPendaftaran";
            // 
            // CUangPangkal
            // 
            this.CUangPangkal.HeaderText = "UangPangkal";
            this.CUangPangkal.Name = "CUangPangkal";
            // 
            // CTerbilang
            // 
            this.CTerbilang.HeaderText = "Terbilang";
            this.CTerbilang.Name = "CTerbilang";
            // 
            // CJumlah
            // 
            this.CJumlah.HeaderText = "Jumlah";
            this.CJumlah.Name = "CJumlah";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(249, 96);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(218, 19);
            this.metroLabel5.TabIndex = 9;
            this.metroLabel5.Text = "Data  Pembayaran Mahasiswa Baru ";
            // 
            // txtNISN
            // 
            // 
            // 
            // 
            this.txtNISN.CustomButton.Image = null;
            this.txtNISN.CustomButton.Location = new System.Drawing.Point(159, 1);
            this.txtNISN.CustomButton.Name = "";
            this.txtNISN.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNISN.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNISN.CustomButton.TabIndex = 1;
            this.txtNISN.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNISN.CustomButton.UseSelectable = true;
            this.txtNISN.CustomButton.Visible = false;
            this.txtNISN.Lines = new string[0];
            this.txtNISN.Location = new System.Drawing.Point(166, 165);
            this.txtNISN.MaxLength = 32767;
            this.txtNISN.Name = "txtNISN";
            this.txtNISN.PasswordChar = '\0';
            this.txtNISN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNISN.SelectedText = "";
            this.txtNISN.SelectionLength = 0;
            this.txtNISN.SelectionStart = 0;
            this.txtNISN.ShortcutsEnabled = true;
            this.txtNISN.Size = new System.Drawing.Size(181, 23);
            this.txtNISN.TabIndex = 22;
            this.txtNISN.UseSelectable = true;
            this.txtNISN.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNISN.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNISN.Click += new System.EventHandler(this.txtNISN_Click);
            this.txtNISN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNISN_KeyDown);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(54, 165);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(102, 19);
            this.metroLabel9.TabIndex = 21;
            this.metroLabel9.Text = "NISN               :";
            // 
            // metroButton1
            // 
            this.metroButton1.BackgroundImage = global::project2.Properties.Resources._797244_check_512x512;
            this.metroButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.metroButton1.Location = new System.Drawing.Point(642, 411);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(59, 59);
            this.metroButton1.TabIndex = 23;
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackgroundImage = global::project2.Properties.Resources.Home_512;
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBack.Location = new System.Drawing.Point(642, 31);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(69, 59);
            this.btnBack.TabIndex = 12;
            this.btnBack.UseSelectable = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackgroundImage = global::project2.Properties.Resources.delete;
            this.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDelete.Location = new System.Drawing.Point(642, 342);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(59, 54);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.UseSelectable = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnupdate
            // 
            this.btnupdate.BackgroundImage = global::project2.Properties.Resources.refresh_rotate_sync_5121;
            this.btnupdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnupdate.Location = new System.Drawing.Point(642, 273);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(59, 53);
            this.btnupdate.TabIndex = 10;
            this.btnupdate.UseSelectable = true;
            this.btnupdate.Click += new System.EventHandler(this.btnupdate_Click);
            // 
            // LihatPembayaranPendaftaran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 484);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.txtNISN);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnupdate);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.dgDataPembayaran);
            this.Controls.Add(this.txtjumlah);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.txtTanggal);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.txtJurusan);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.txtNama);
            this.Controls.Add(this.metroLabel1);
            this.Name = "LihatPembayaranPendaftaran";
            this.Text = "PembayaranPendaftaran";
            this.Load += new System.EventHandler(this.PembayaranPendaftaran_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgDataPembayaran)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtNama;
        private MetroFramework.Controls.MetroTextBox txtJurusan;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtTanggal;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtjumlah;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroGrid dgDataPembayaran;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroButton btnupdate;
        private MetroFramework.Controls.MetroButton btnDelete;
        private MetroFramework.Controls.MetroButton btnBack;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNISN;
        private System.Windows.Forms.DataGridViewTextBoxColumn CJumlah;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTerbilang;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUangPangkal;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUangPendaftaran;
        private System.Windows.Forms.DataGridViewTextBoxColumn CTanggalBayar;
        private System.Windows.Forms.DataGridViewTextBoxColumn CJurusan;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNama;
        private MetroFramework.Controls.MetroTextBox txtNISN;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroButton metroButton1;
    }
}