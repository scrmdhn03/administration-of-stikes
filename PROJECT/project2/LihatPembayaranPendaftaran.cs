﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.IO;

namespace project2
{
    public partial class LihatPembayaranPendaftaran : MetroForm
    {
        public LihatPembayaranPendaftaran()
        {
            InitializeComponent();
        }

        public void Display()
        {
            dgDataPembayaran.Rows.Clear();
            var Lines = File.ReadAllLines("DataPembayaran.txt");
            if (Lines.Count() > 0)
            {
                foreach (var cellArray in Lines)
                {
                    var cell = cellArray.Split('#');
                    if (cell.Length == dgDataPembayaran.Columns.Count)

                        dgDataPembayaran.Rows.Add(cell);
                }
            }

        }

        private void metroLabel2_Click(object sender, EventArgs e)
        {

        }

        private void metroTextBox2_Click(object sender, EventArgs e)
        {

        }

        private void PembayaranPendaftaran_Load(object sender, EventArgs e)
        {
            Display();
        }

        private void txtNama_Click(object sender, EventArgs e)
        {

        }

        private void txtNama_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) // kalo di enter jadi langsung masuk ke yg baru di cari gak kosong dl
                try
                {
                    dgDataPembayaran.Rows.Clear();
                    var Lines = File.ReadAllLines("DataPembayaran.txt");
                    if (Lines.Count() > 0)
                    {
                        foreach (var cellArray in Lines)
                        {
                            var cell = cellArray.Split('#');

                            if (cell.Contains(txtNISN.Text))
                            {
                                if (cell.Length == dgDataPembayaran.Columns.Count)
                                    dgDataPembayaran.Rows.Add(cell);
                                txtNama.Text = cell[1];
                                txtJurusan.Text = cell[2];
                                txtTanggal.Text = cell[3];
                                txtjumlah.Text = cell[7];


                            }


                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> newfile = new List<string>();
                string temp = "";
                dgDataPembayaran.Rows.Clear();
                var lines = File.ReadAllLines("DataPembayaran.txt");

                foreach (var cellArray in lines)
                {
                    var data = cellArray.Split('#');
                    if (data[0].Equals(txtNama.Text))
                    {
                        data[1] = txtJurusan.Text.Trim(); //jika yg di ubah banyak ini nya banyak
                        data[2] = txtTanggal.Text;
                        data[3] = txtjumlah.Text;

                    }
                    newfile.Add(string.Join("#", data));
                }
                File.WriteAllLines("DataPembayaran.txt", newfile);

                MessageBox.Show("Data has been updated");
                Display();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder newFile = new StringBuilder();
                string temp = "";
                dgDataPembayaran.Rows.Clear();
                var lines = File.ReadAllLines("DataPembayaran.txt");
                foreach (var cellArray in lines)
                {
                    if (cellArray.Contains(txtNama.Text))
                    {
                        temp = cellArray.Remove(0);
                        continue;

                    }
                    newFile.Append(cellArray + "\r\n");

                }
                File.WriteAllText("DataPembayaran.txt", newFile.ToString());
                MessageBox.Show("Data Has been deleted successfully");
                Display();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgDataPembayaran_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            MenuKurikulum obj = new MenuKurikulum();
            obj.Show();
            this.Hide();
        }

        private void txtNISN_Click(object sender, EventArgs e)
        {

        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            RPTPembayaran obj = new RPTPembayaran();
            obj.Show();
        }

        private void txtNISN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) // kalo di enter jadi langsung masuk ke yg baru di cari gak kosong dl
                try
                {
                    dgDataPembayaran.Rows.Clear();
                    var Lines = File.ReadAllLines("DataPembayaran.txt");
                    if (Lines.Count() > 0)
                    {
                        foreach (var cellArray in Lines)
                        {
                            var cell = cellArray.Split('#');

                            if (cell.Contains(txtNISN.Text))
                            {
                                if (cell.Length == dgDataPembayaran.Columns.Count)
                                    dgDataPembayaran.Rows.Add(cell);
                                txtNama.Text = cell[1];

                                txtJurusan.Text = cell[2];
                                txtTanggal.Text = cell[3];
                                txtjumlah.Text = cell[7];
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

        }
              
    }
}
