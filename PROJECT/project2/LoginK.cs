﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using MetroFramework.Forms;

namespace project2
{
    public partial class LoginK : MetroForm
    {
        public LoginK()
        {
            InitializeComponent();
        }

        private void LoginK_Load(object sender, EventArgs e)
        {

        }

        private void Logi1_Click(object sender, EventArgs e)
        {
            string ID, password;
            ID = txtID.Text;
            password = txtPassword.Text;
            if ((ID.Length == 0) || (password.Length == 0))
            {
                MessageBox.Show("ID or password cannot left blank");
            }
            else
            {
                if ((ID == "kurikulum") && (password == "kurikulum123"))
                {
                    MessageBox.Show("Welcome");
                    MenuKurikulum obj = new MenuKurikulum();
                    obj.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Invalid ID or password");
                    txtID.Clear();
                    txtPassword.Clear();
                }
            }

        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                Logi1_Click(sender, e);
            }
        }
    }
}
