﻿namespace project2
{
    partial class MenuKurikulum
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDaftarD3 = new MetroFramework.Controls.MetroButton();
            this.btnDataD3 = new MetroFramework.Controls.MetroButton();
            this.btnDataPembayaran = new MetroFramework.Controls.MetroButton();
            this.btnMenuUtama = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // btnDaftarD3
            // 
            this.btnDaftarD3.Location = new System.Drawing.Point(194, 85);
            this.btnDaftarD3.Name = "btnDaftarD3";
            this.btnDaftarD3.Size = new System.Drawing.Size(206, 39);
            this.btnDaftarD3.TabIndex = 0;
            this.btnDaftarD3.Text = "Pendaftaran Mahasiswa Baru";
            this.btnDaftarD3.UseSelectable = true;
            this.btnDaftarD3.Click += new System.EventHandler(this.btnDaftarD3_Click);
            // 
            // btnDataD3
            // 
            this.btnDataD3.Location = new System.Drawing.Point(194, 161);
            this.btnDataD3.Name = "btnDataD3";
            this.btnDataD3.Size = new System.Drawing.Size(206, 37);
            this.btnDataD3.TabIndex = 1;
            this.btnDataD3.Text = "Data Mahasiswa Baru";
            this.btnDataD3.UseSelectable = true;
            this.btnDataD3.Click += new System.EventHandler(this.btnDataD3_Click);
            // 
            // btnDataPembayaran
            // 
            this.btnDataPembayaran.Location = new System.Drawing.Point(194, 226);
            this.btnDataPembayaran.Name = "btnDataPembayaran";
            this.btnDataPembayaran.Size = new System.Drawing.Size(206, 38);
            this.btnDataPembayaran.TabIndex = 2;
            this.btnDataPembayaran.Text = "Data Pembayaran";
            this.btnDataPembayaran.UseSelectable = true;
            this.btnDataPembayaran.Click += new System.EventHandler(this.btnDataPembayaran_Click);
            // 
            // btnMenuUtama
            // 
            this.btnMenuUtama.BackgroundImage = global::project2.Properties.Resources.ICON_Residence;
            this.btnMenuUtama.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMenuUtama.Location = new System.Drawing.Point(23, 257);
            this.btnMenuUtama.Name = "btnMenuUtama";
            this.btnMenuUtama.Size = new System.Drawing.Size(79, 58);
            this.btnMenuUtama.TabIndex = 3;
            this.btnMenuUtama.UseSelectable = true;
            this.btnMenuUtama.Click += new System.EventHandler(this.btnMenuUtama_Click);
            // 
            // MenuKurikulum
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 338);
            this.Controls.Add(this.btnMenuUtama);
            this.Controls.Add(this.btnDataPembayaran);
            this.Controls.Add(this.btnDataD3);
            this.Controls.Add(this.btnDaftarD3);
            this.Name = "MenuKurikulum";
            this.Text = "MenuKurikulum";
            this.Load += new System.EventHandler(this.MenuKurikulum_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroButton btnDaftarD3;
        private MetroFramework.Controls.MetroButton btnDataD3;
        private MetroFramework.Controls.MetroButton btnDataPembayaran;
        private MetroFramework.Controls.MetroButton btnMenuUtama;
    }
}