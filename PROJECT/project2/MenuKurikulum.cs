﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace project2
{
    public partial class MenuKurikulum : MetroForm
    {
        public MenuKurikulum()
        {
            InitializeComponent();
        }

        private void MenuKurikulum_Load(object sender, EventArgs e)
        {

        }

        private void btnDaftarD3_Click(object sender, EventArgs e)
        {
            Pendaftaran obj = new Pendaftaran();
            obj.Show();
            this.Hide();
        }

        private void btnDataD3_Click(object sender, EventArgs e)
        {
           LihatDataMahasiswa obj = new LihatDataMahasiswa();
            obj.Show();
            this.Hide();
        }

        private void btnDataPembayaran_Click(object sender, EventArgs e)
        {
            LihatPembayaranPendaftaran obj = new LihatPembayaranPendaftaran();
            obj.Show();
            this.Hide();
        }

        private void btnMenuUtama_Click(object sender, EventArgs e)
        {
            MenuUtama obj = new MenuUtama();
            obj.Show();
            this.Hide();
        }
    }
}
