﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using S2RUniversity;


namespace project2
{
    public partial class MenuUtama : MetroForm
    {
        public MenuUtama()
        {
            InitializeComponent();
        }

        private void btnPendaftaran_Click(object sender, EventArgs e)
        {
            LoginK obj = new LoginK();
            obj.Show();
            this.Hide();
        }
        public S2RUniversity.LoginAdmin frmSuci = new S2RUniversity.LoginAdmin();
        private void MenuUtama_Load(object sender, EventArgs e)
        {

        }

        private void btnAdmin_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmSuci.Show(); 
        }
    }
}
