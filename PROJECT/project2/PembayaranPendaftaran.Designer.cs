﻿namespace project2
{
    partial class PembayaranPendaftaran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtNama = new MetroFramework.Controls.MetroTextBox();
            this.txtJurusan = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtUangDaftar = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtUangPangkal = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.txttotalterbilang = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtjumlah = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.TanggalBayar = new MetroFramework.Controls.MetroDateTime();
            this.txtNISN = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.Button2 = new MetroFramework.Controls.MetroButton();
            this.Button1 = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(59, 160);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(144, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Nama                        :";
            // 
            // txtNama
            // 
            // 
            // 
            // 
            this.txtNama.CustomButton.Image = null;
            this.txtNama.CustomButton.Location = new System.Drawing.Point(103, 1);
            this.txtNama.CustomButton.Name = "";
            this.txtNama.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNama.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNama.CustomButton.TabIndex = 1;
            this.txtNama.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNama.CustomButton.UseSelectable = true;
            this.txtNama.CustomButton.Visible = false;
            this.txtNama.Lines = new string[0];
            this.txtNama.Location = new System.Drawing.Point(249, 156);
            this.txtNama.MaxLength = 32767;
            this.txtNama.Name = "txtNama";
            this.txtNama.PasswordChar = '\0';
            this.txtNama.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNama.SelectedText = "";
            this.txtNama.SelectionLength = 0;
            this.txtNama.SelectionStart = 0;
            this.txtNama.ShortcutsEnabled = true;
            this.txtNama.Size = new System.Drawing.Size(125, 23);
            this.txtNama.TabIndex = 1;
            this.txtNama.UseSelectable = true;
            this.txtNama.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNama.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNama.Click += new System.EventHandler(this.txtNama_Click);
            // 
            // txtJurusan
            // 
            // 
            // 
            // 
            this.txtJurusan.CustomButton.Image = null;
            this.txtJurusan.CustomButton.Location = new System.Drawing.Point(103, 1);
            this.txtJurusan.CustomButton.Name = "";
            this.txtJurusan.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtJurusan.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtJurusan.CustomButton.TabIndex = 1;
            this.txtJurusan.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtJurusan.CustomButton.UseSelectable = true;
            this.txtJurusan.CustomButton.Visible = false;
            this.txtJurusan.Lines = new string[0];
            this.txtJurusan.Location = new System.Drawing.Point(249, 197);
            this.txtJurusan.MaxLength = 32767;
            this.txtJurusan.Name = "txtJurusan";
            this.txtJurusan.PasswordChar = '\0';
            this.txtJurusan.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtJurusan.SelectedText = "";
            this.txtJurusan.SelectionLength = 0;
            this.txtJurusan.SelectionStart = 0;
            this.txtJurusan.ShortcutsEnabled = true;
            this.txtJurusan.Size = new System.Drawing.Size(125, 23);
            this.txtJurusan.TabIndex = 3;
            this.txtJurusan.UseSelectable = true;
            this.txtJurusan.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtJurusan.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtJurusan.Click += new System.EventHandler(this.txtJurusan_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(59, 197);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(147, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Jurusan                       :";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(57, 233);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(146, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Tanggal Pembayaran   :";
            // 
            // txtUangDaftar
            // 
            // 
            // 
            // 
            this.txtUangDaftar.CustomButton.Image = null;
            this.txtUangDaftar.CustomButton.Location = new System.Drawing.Point(103, 1);
            this.txtUangDaftar.CustomButton.Name = "";
            this.txtUangDaftar.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtUangDaftar.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtUangDaftar.CustomButton.TabIndex = 1;
            this.txtUangDaftar.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtUangDaftar.CustomButton.UseSelectable = true;
            this.txtUangDaftar.CustomButton.Visible = false;
            this.txtUangDaftar.Lines = new string[0];
            this.txtUangDaftar.Location = new System.Drawing.Point(249, 269);
            this.txtUangDaftar.MaxLength = 32767;
            this.txtUangDaftar.Name = "txtUangDaftar";
            this.txtUangDaftar.PasswordChar = '\0';
            this.txtUangDaftar.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUangDaftar.SelectedText = "";
            this.txtUangDaftar.SelectionLength = 0;
            this.txtUangDaftar.SelectionStart = 0;
            this.txtUangDaftar.ShortcutsEnabled = true;
            this.txtUangDaftar.Size = new System.Drawing.Size(125, 23);
            this.txtUangDaftar.TabIndex = 7;
            this.txtUangDaftar.UseSelectable = true;
            this.txtUangDaftar.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtUangDaftar.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(59, 269);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(141, 19);
            this.metroLabel4.TabIndex = 6;
            this.metroLabel4.Text = "Uang Pendaftaran      :";
            // 
            // txtUangPangkal
            // 
            // 
            // 
            // 
            this.txtUangPangkal.CustomButton.Image = null;
            this.txtUangPangkal.CustomButton.Location = new System.Drawing.Point(103, 1);
            this.txtUangPangkal.CustomButton.Name = "";
            this.txtUangPangkal.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtUangPangkal.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtUangPangkal.CustomButton.TabIndex = 1;
            this.txtUangPangkal.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtUangPangkal.CustomButton.UseSelectable = true;
            this.txtUangPangkal.CustomButton.Visible = false;
            this.txtUangPangkal.Lines = new string[0];
            this.txtUangPangkal.Location = new System.Drawing.Point(249, 306);
            this.txtUangPangkal.MaxLength = 32767;
            this.txtUangPangkal.Name = "txtUangPangkal";
            this.txtUangPangkal.PasswordChar = '\0';
            this.txtUangPangkal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUangPangkal.SelectedText = "";
            this.txtUangPangkal.SelectionLength = 0;
            this.txtUangPangkal.SelectionStart = 0;
            this.txtUangPangkal.ShortcutsEnabled = true;
            this.txtUangPangkal.Size = new System.Drawing.Size(125, 23);
            this.txtUangPangkal.TabIndex = 9;
            this.txtUangPangkal.UseSelectable = true;
            this.txtUangPangkal.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtUangPangkal.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(59, 305);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(144, 19);
            this.metroLabel5.TabIndex = 8;
            this.metroLabel5.Text = "Uang Pangkal             :";
            // 
            // txttotalterbilang
            // 
            // 
            // 
            // 
            this.txttotalterbilang.CustomButton.Image = null;
            this.txttotalterbilang.CustomButton.Location = new System.Drawing.Point(103, 1);
            this.txttotalterbilang.CustomButton.Name = "";
            this.txttotalterbilang.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txttotalterbilang.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txttotalterbilang.CustomButton.TabIndex = 1;
            this.txttotalterbilang.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txttotalterbilang.CustomButton.UseSelectable = true;
            this.txttotalterbilang.CustomButton.Visible = false;
            this.txttotalterbilang.Lines = new string[0];
            this.txttotalterbilang.Location = new System.Drawing.Point(249, 346);
            this.txttotalterbilang.MaxLength = 32767;
            this.txttotalterbilang.Name = "txttotalterbilang";
            this.txttotalterbilang.PasswordChar = '\0';
            this.txttotalterbilang.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txttotalterbilang.SelectedText = "";
            this.txttotalterbilang.SelectionLength = 0;
            this.txttotalterbilang.SelectionStart = 0;
            this.txttotalterbilang.ShortcutsEnabled = true;
            this.txttotalterbilang.Size = new System.Drawing.Size(125, 23);
            this.txttotalterbilang.TabIndex = 11;
            this.txttotalterbilang.UseSelectable = true;
            this.txttotalterbilang.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txttotalterbilang.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(59, 350);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(140, 19);
            this.metroLabel6.TabIndex = 10;
            this.metroLabel6.Text = "Total Terbilang           :";
            // 
            // txtjumlah
            // 
            // 
            // 
            // 
            this.txtjumlah.CustomButton.Image = null;
            this.txtjumlah.CustomButton.Location = new System.Drawing.Point(103, 1);
            this.txtjumlah.CustomButton.Name = "";
            this.txtjumlah.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtjumlah.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtjumlah.CustomButton.TabIndex = 1;
            this.txtjumlah.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtjumlah.CustomButton.UseSelectable = true;
            this.txtjumlah.CustomButton.Visible = false;
            this.txtjumlah.Lines = new string[0];
            this.txtjumlah.Location = new System.Drawing.Point(249, 385);
            this.txtjumlah.MaxLength = 32767;
            this.txtjumlah.Name = "txtjumlah";
            this.txtjumlah.PasswordChar = '\0';
            this.txtjumlah.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtjumlah.SelectedText = "";
            this.txtjumlah.SelectionLength = 0;
            this.txtjumlah.SelectionStart = 0;
            this.txtjumlah.ShortcutsEnabled = true;
            this.txtjumlah.Size = new System.Drawing.Size(125, 23);
            this.txtjumlah.TabIndex = 13;
            this.txtjumlah.UseSelectable = true;
            this.txtjumlah.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtjumlah.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(57, 385);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(141, 19);
            this.metroLabel7.TabIndex = 12;
            this.metroLabel7.Text = "Jumlah                      :";
            // 
            // TanggalBayar
            // 
            this.TanggalBayar.Location = new System.Drawing.Point(249, 233);
            this.TanggalBayar.MinimumSize = new System.Drawing.Size(0, 29);
            this.TanggalBayar.Name = "TanggalBayar";
            this.TanggalBayar.Size = new System.Drawing.Size(125, 29);
            this.TanggalBayar.TabIndex = 14;
            // 
            // txtNISN
            // 
            // 
            // 
            // 
            this.txtNISN.CustomButton.Image = null;
            this.txtNISN.CustomButton.Location = new System.Drawing.Point(103, 1);
            this.txtNISN.CustomButton.Name = "";
            this.txtNISN.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNISN.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNISN.CustomButton.TabIndex = 1;
            this.txtNISN.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNISN.CustomButton.UseSelectable = true;
            this.txtNISN.CustomButton.Visible = false;
            this.txtNISN.Lines = new string[0];
            this.txtNISN.Location = new System.Drawing.Point(247, 117);
            this.txtNISN.MaxLength = 32767;
            this.txtNISN.Name = "txtNISN";
            this.txtNISN.PasswordChar = '\0';
            this.txtNISN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNISN.SelectedText = "";
            this.txtNISN.SelectionLength = 0;
            this.txtNISN.SelectionStart = 0;
            this.txtNISN.ShortcutsEnabled = true;
            this.txtNISN.Size = new System.Drawing.Size(125, 23);
            this.txtNISN.TabIndex = 20;
            this.txtNISN.UseSelectable = true;
            this.txtNISN.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNISN.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNISN.Click += new System.EventHandler(this.txtNISN_Click);
            this.txtNISN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNISN_KeyDown);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(57, 121);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(142, 19);
            this.metroLabel9.TabIndex = 19;
            this.metroLabel9.Text = "NISN                         :";
            // 
            // btnBack
            // 
            this.btnBack.BackgroundImage = global::project2.Properties.Resources.Home_512;
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBack.Location = new System.Drawing.Point(455, 9);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(41, 31);
            this.btnBack.TabIndex = 17;
            this.btnBack.UseSelectable = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // Button2
            // 
            this.Button2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Button2.BackgroundImage = global::project2.Properties.Resources.Print;
            this.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button2.ForeColor = System.Drawing.Color.Cyan;
            this.Button2.Location = new System.Drawing.Point(412, 9);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(37, 31);
            this.Button2.TabIndex = 16;
            this.Button2.UseSelectable = true;
            this.Button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // Button1
            // 
            this.Button1.BackgroundImage = global::project2.Properties.Resources.Icon_CheckMark_300x300;
            this.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button1.DisplayFocus = true;
            this.Button1.ForeColor = System.Drawing.Color.PaleTurquoise;
            this.Button1.Location = new System.Drawing.Point(429, 427);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(35, 28);
            this.Button1.TabIndex = 15;
            this.Button1.UseSelectable = true;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // PembayaranPendaftaran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 478);
            this.Controls.Add(this.txtNISN);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.TanggalBayar);
            this.Controls.Add(this.txtjumlah);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.txttotalterbilang);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.txtUangPangkal);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.txtUangDaftar);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.txtJurusan);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.txtNama);
            this.Controls.Add(this.metroLabel1);
            this.ForeColor = System.Drawing.Color.LightBlue;
            this.Name = "PembayaranPendaftaran";
            this.Text = "PembayaranPendaftaran";
            this.TransparencyKey = System.Drawing.Color.PowderBlue;
            this.Load += new System.EventHandler(this.PembayaranPendaftaran_Load);
            this.BackColorChanged += new System.EventHandler(this.PembayaranPendaftaran_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtNama;
        private MetroFramework.Controls.MetroTextBox txtJurusan;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtUangDaftar;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtUangPangkal;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox txttotalterbilang;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtjumlah;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroDateTime TanggalBayar;
        private MetroFramework.Controls.MetroButton Button1;
        private MetroFramework.Controls.MetroButton Button2;
        private MetroFramework.Controls.MetroButton btnBack;
        private MetroFramework.Controls.MetroTextBox txtNISN;
        private MetroFramework.Controls.MetroLabel metroLabel9;
    }
}