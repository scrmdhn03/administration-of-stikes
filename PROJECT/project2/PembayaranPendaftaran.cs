﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.IO;

namespace project2
{
    public partial class PembayaranPendaftaran : MetroForm
    {
        public PembayaranPendaftaran()
        {
            InitializeComponent();
        }
        
        private void txtNama_Click(object sender, EventArgs e)
        {
            try
            {

                var Lines = File.ReadAllLines("DataPendaftarD3.txt");
                if (Lines.Count() > 0)
                {
                    foreach (var cellArray in Lines)
                    {
                        var cell = cellArray.Split('#');

                        if (cell.Contains(txtNISN.Text))
                        {

                            txtJurusan.Text = cell[10];

                        }


                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtJurusan_Click(object sender, EventArgs e)
        {

           
        }

        public static string Nama, Jurusan, Terbilang, Jumlah, Tanggal;

        private void Button2_Click(object sender, EventArgs e)
        {
            BuktiPembayaran obj = new BuktiPembayaran();
            obj.Show();
            this.Hide();
        }

        private void txtNISN_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtNISN_Click(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string Bayar;
            

            FileStream fs = new FileStream("DataPembayaran.txt", FileMode.Append, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);

            Bayar =  txtNISN.Text + "#" + txtNama.Text + "#" + txtJurusan.Text + "#" + TanggalBayar.Text + "#" + txtUangDaftar.Text + "#" + txtUangPangkal.Text + "#" + txttotalterbilang.Text + "#" + txtjumlah.Text;
            sw.WriteLine(Bayar);
            sw.Flush();
            sw.Close();
            fs.Close();


            MessageBox.Show("Berhasil Daftar", "information", MessageBoxButtons.OK, MessageBoxIcon.Information); //buat yang are you sure gtgt kalo messageboxbuttons
            Nama = txtNama.Text;
            Jurusan = txtJurusan.Text;
            Terbilang = txttotalterbilang.Text;
            Jumlah = txtjumlah.Text;
            Tanggal = TanggalBayar.Text;
                                                                                                              //Display();
            txtNama.Clear();
            txtUangDaftar.Clear();
            txtUangPangkal.Clear();
            txttotalterbilang.Clear();
            txtjumlah.Clear();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            MenuKurikulum obj = new MenuKurikulum();
            obj.Show();
            this.Hide();
        }

        private void PembayaranPendaftaran_Load(object sender, EventArgs e)
        {
            txtNama.Text = Pendaftaran.Nama;
            txtJurusan.Text = Pendaftaran.Jurusan;
            txtNISN.Text = Pendaftaran.NISN;
            if (txtJurusan.Text != null)
            {

                txtUangDaftar.Enabled = false;
                txtUangPangkal.Enabled = false;
                txttotalterbilang.Enabled = false;
                txtjumlah.Enabled = false;
                int uangPangkal = 5000000;
                int uangDaftar = 100000;
                string terbilang = "Lima Juta Seratus Ribu Rupiah";
                int jumlah = uangPangkal + uangDaftar;
                txttotalterbilang.Text = terbilang.ToString();
                txtjumlah.Text = jumlah.ToString();
                txtUangDaftar.Text = uangDaftar.ToString();
                txtUangPangkal.Text = uangPangkal.ToString();
            }
        }
    }
}
