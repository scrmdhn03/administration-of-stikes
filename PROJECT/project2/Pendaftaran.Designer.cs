﻿namespace project2
{
    partial class Pendaftaran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new MetroFramework.Controls.MetroLabel();
            this.txtNISN = new MetroFramework.Controls.MetroTextBox();
            this.txtNama = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtAgama = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtAlamat = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtNoTelp = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.TTL = new MetroFramework.Controls.MetroDateTime();
            this.cJK = new MetroFramework.Controls.MetroComboBox();
            this.txtNamaOrtu = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.txtAlamatOrtu = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.txtAsalSekolah = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.cJurusan = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.btnSimpan = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(69, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "NISN                   :";
            // 
            // txtNISN
            // 
            // 
            // 
            // 
            this.txtNISN.CustomButton.Image = null;
            this.txtNISN.CustomButton.Location = new System.Drawing.Point(89, 1);
            this.txtNISN.CustomButton.Name = "";
            this.txtNISN.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNISN.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNISN.CustomButton.TabIndex = 1;
            this.txtNISN.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNISN.CustomButton.UseSelectable = true;
            this.txtNISN.CustomButton.Visible = false;
            this.txtNISN.Lines = new string[0];
            this.txtNISN.Location = new System.Drawing.Point(240, 123);
            this.txtNISN.MaxLength = 32767;
            this.txtNISN.Name = "txtNISN";
            this.txtNISN.PasswordChar = '\0';
            this.txtNISN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNISN.SelectedText = "";
            this.txtNISN.SelectionLength = 0;
            this.txtNISN.SelectionStart = 0;
            this.txtNISN.ShortcutsEnabled = true;
            this.txtNISN.Size = new System.Drawing.Size(111, 23);
            this.txtNISN.TabIndex = 1;
            this.txtNISN.UseSelectable = true;
            this.txtNISN.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNISN.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNISN.Leave += new System.EventHandler(this.txtNISN_Leave);
            // 
            // txtNama
            // 
            // 
            // 
            // 
            this.txtNama.CustomButton.Image = null;
            this.txtNama.CustomButton.Location = new System.Drawing.Point(89, 1);
            this.txtNama.CustomButton.Name = "";
            this.txtNama.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNama.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNama.CustomButton.TabIndex = 1;
            this.txtNama.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNama.CustomButton.UseSelectable = true;
            this.txtNama.CustomButton.Visible = false;
            this.txtNama.Lines = new string[0];
            this.txtNama.Location = new System.Drawing.Point(240, 164);
            this.txtNama.MaxLength = 32767;
            this.txtNama.Name = "txtNama";
            this.txtNama.PasswordChar = '\0';
            this.txtNama.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNama.SelectedText = "";
            this.txtNama.SelectionLength = 0;
            this.txtNama.SelectionStart = 0;
            this.txtNama.ShortcutsEnabled = true;
            this.txtNama.Size = new System.Drawing.Size(111, 23);
            this.txtNama.TabIndex = 3;
            this.txtNama.UseSelectable = true;
            this.txtNama.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNama.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(69, 168);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(120, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Nama                  :";
            // 
            // txtAgama
            // 
            // 
            // 
            // 
            this.txtAgama.CustomButton.Image = null;
            this.txtAgama.CustomButton.Location = new System.Drawing.Point(89, 1);
            this.txtAgama.CustomButton.Name = "";
            this.txtAgama.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtAgama.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAgama.CustomButton.TabIndex = 1;
            this.txtAgama.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAgama.CustomButton.UseSelectable = true;
            this.txtAgama.CustomButton.Visible = false;
            this.txtAgama.Lines = new string[0];
            this.txtAgama.Location = new System.Drawing.Point(240, 296);
            this.txtAgama.MaxLength = 32767;
            this.txtAgama.Name = "txtAgama";
            this.txtAgama.PasswordChar = '\0';
            this.txtAgama.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAgama.SelectedText = "";
            this.txtAgama.SelectionLength = 0;
            this.txtAgama.SelectionStart = 0;
            this.txtAgama.ShortcutsEnabled = true;
            this.txtAgama.Size = new System.Drawing.Size(111, 23);
            this.txtAgama.TabIndex = 5;
            this.txtAgama.UseSelectable = true;
            this.txtAgama.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAgama.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(67, 213);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(117, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Tanggal Lahir       :";
            // 
            // txtAlamat
            // 
            // 
            // 
            // 
            this.txtAlamat.CustomButton.Image = null;
            this.txtAlamat.CustomButton.Location = new System.Drawing.Point(91, 1);
            this.txtAlamat.CustomButton.Name = "";
            this.txtAlamat.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtAlamat.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAlamat.CustomButton.TabIndex = 1;
            this.txtAlamat.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAlamat.CustomButton.UseSelectable = true;
            this.txtAlamat.CustomButton.Visible = false;
            this.txtAlamat.Lines = new string[0];
            this.txtAlamat.Location = new System.Drawing.Point(238, 346);
            this.txtAlamat.MaxLength = 32767;
            this.txtAlamat.Name = "txtAlamat";
            this.txtAlamat.PasswordChar = '\0';
            this.txtAlamat.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAlamat.SelectedText = "";
            this.txtAlamat.SelectionLength = 0;
            this.txtAlamat.SelectionStart = 0;
            this.txtAlamat.ShortcutsEnabled = true;
            this.txtAlamat.Size = new System.Drawing.Size(113, 23);
            this.txtAlamat.TabIndex = 7;
            this.txtAlamat.UseSelectable = true;
            this.txtAlamat.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAlamat.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(70, 250);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(117, 19);
            this.metroLabel4.TabIndex = 6;
            this.metroLabel4.Text = "Jenis Kelamin       :";
            // 
            // txtNoTelp
            // 
            // 
            // 
            // 
            this.txtNoTelp.CustomButton.Image = null;
            this.txtNoTelp.CustomButton.Location = new System.Drawing.Point(91, 1);
            this.txtNoTelp.CustomButton.Name = "";
            this.txtNoTelp.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNoTelp.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNoTelp.CustomButton.TabIndex = 1;
            this.txtNoTelp.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNoTelp.CustomButton.UseSelectable = true;
            this.txtNoTelp.CustomButton.Visible = false;
            this.txtNoTelp.Lines = new string[0];
            this.txtNoTelp.Location = new System.Drawing.Point(238, 390);
            this.txtNoTelp.MaxLength = 32767;
            this.txtNoTelp.Name = "txtNoTelp";
            this.txtNoTelp.PasswordChar = '\0';
            this.txtNoTelp.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNoTelp.SelectedText = "";
            this.txtNoTelp.SelectionLength = 0;
            this.txtNoTelp.SelectionStart = 0;
            this.txtNoTelp.ShortcutsEnabled = true;
            this.txtNoTelp.Size = new System.Drawing.Size(113, 23);
            this.txtNoTelp.TabIndex = 9;
            this.txtNoTelp.UseSelectable = true;
            this.txtNoTelp.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNoTelp.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNoTelp.Leave += new System.EventHandler(this.txtNoTelp_Leave);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(67, 300);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(119, 19);
            this.metroLabel5.TabIndex = 8;
            this.metroLabel5.Text = "Agama                :";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(67, 350);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(114, 19);
            this.metroLabel6.TabIndex = 10;
            this.metroLabel6.Text = "Alamat               :";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(69, 390);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(112, 19);
            this.metroLabel1.TabIndex = 11;
            this.metroLabel1.Text = "No. Telp             :";
            // 
            // TTL
            // 
            this.TTL.Location = new System.Drawing.Point(238, 203);
            this.TTL.MinimumSize = new System.Drawing.Size(0, 29);
            this.TTL.Name = "TTL";
            this.TTL.Size = new System.Drawing.Size(113, 29);
            this.TTL.TabIndex = 12;
            // 
            // cJK
            // 
            this.cJK.FormattingEnabled = true;
            this.cJK.ItemHeight = 23;
            this.cJK.Items.AddRange(new object[] {
            "Perempuan",
            "Laki-Laki"});
            this.cJK.Location = new System.Drawing.Point(241, 250);
            this.cJK.Name = "cJK";
            this.cJK.Size = new System.Drawing.Size(113, 29);
            this.cJK.TabIndex = 13;
            this.cJK.UseSelectable = true;
            // 
            // txtNamaOrtu
            // 
            // 
            // 
            // 
            this.txtNamaOrtu.CustomButton.Image = null;
            this.txtNamaOrtu.CustomButton.Location = new System.Drawing.Point(89, 1);
            this.txtNamaOrtu.CustomButton.Name = "";
            this.txtNamaOrtu.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNamaOrtu.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNamaOrtu.CustomButton.TabIndex = 1;
            this.txtNamaOrtu.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNamaOrtu.CustomButton.UseSelectable = true;
            this.txtNamaOrtu.CustomButton.Visible = false;
            this.txtNamaOrtu.Lines = new string[0];
            this.txtNamaOrtu.Location = new System.Drawing.Point(589, 119);
            this.txtNamaOrtu.MaxLength = 32767;
            this.txtNamaOrtu.Name = "txtNamaOrtu";
            this.txtNamaOrtu.PasswordChar = '\0';
            this.txtNamaOrtu.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNamaOrtu.SelectedText = "";
            this.txtNamaOrtu.SelectionLength = 0;
            this.txtNamaOrtu.SelectionStart = 0;
            this.txtNamaOrtu.ShortcutsEnabled = true;
            this.txtNamaOrtu.Size = new System.Drawing.Size(111, 23);
            this.txtNamaOrtu.TabIndex = 15;
            this.txtNamaOrtu.UseSelectable = true;
            this.txtNamaOrtu.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNamaOrtu.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(410, 123);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(129, 19);
            this.metroLabel7.TabIndex = 14;
            this.metroLabel7.Text = "Nama Orang Tua    :";
            // 
            // txtAlamatOrtu
            // 
            // 
            // 
            // 
            this.txtAlamatOrtu.CustomButton.Image = null;
            this.txtAlamatOrtu.CustomButton.Location = new System.Drawing.Point(89, 1);
            this.txtAlamatOrtu.CustomButton.Name = "";
            this.txtAlamatOrtu.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtAlamatOrtu.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAlamatOrtu.CustomButton.TabIndex = 1;
            this.txtAlamatOrtu.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAlamatOrtu.CustomButton.UseSelectable = true;
            this.txtAlamatOrtu.CustomButton.Visible = false;
            this.txtAlamatOrtu.Lines = new string[0];
            this.txtAlamatOrtu.Location = new System.Drawing.Point(589, 168);
            this.txtAlamatOrtu.MaxLength = 32767;
            this.txtAlamatOrtu.Name = "txtAlamatOrtu";
            this.txtAlamatOrtu.PasswordChar = '\0';
            this.txtAlamatOrtu.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAlamatOrtu.SelectedText = "";
            this.txtAlamatOrtu.SelectionLength = 0;
            this.txtAlamatOrtu.SelectionStart = 0;
            this.txtAlamatOrtu.ShortcutsEnabled = true;
            this.txtAlamatOrtu.Size = new System.Drawing.Size(111, 23);
            this.txtAlamatOrtu.TabIndex = 17;
            this.txtAlamatOrtu.UseSelectable = true;
            this.txtAlamatOrtu.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAlamatOrtu.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(410, 168);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(131, 19);
            this.metroLabel8.TabIndex = 16;
            this.metroLabel8.Text = "Alamat Orang Tua   :";
            // 
            // txtAsalSekolah
            // 
            // 
            // 
            // 
            this.txtAsalSekolah.CustomButton.Image = null;
            this.txtAsalSekolah.CustomButton.Location = new System.Drawing.Point(89, 1);
            this.txtAsalSekolah.CustomButton.Name = "";
            this.txtAsalSekolah.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtAsalSekolah.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAsalSekolah.CustomButton.TabIndex = 1;
            this.txtAsalSekolah.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAsalSekolah.CustomButton.UseSelectable = true;
            this.txtAsalSekolah.CustomButton.Visible = false;
            this.txtAsalSekolah.Lines = new string[0];
            this.txtAsalSekolah.Location = new System.Drawing.Point(589, 213);
            this.txtAsalSekolah.MaxLength = 32767;
            this.txtAsalSekolah.Name = "txtAsalSekolah";
            this.txtAsalSekolah.PasswordChar = '\0';
            this.txtAsalSekolah.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAsalSekolah.SelectedText = "";
            this.txtAsalSekolah.SelectionLength = 0;
            this.txtAsalSekolah.SelectionStart = 0;
            this.txtAsalSekolah.ShortcutsEnabled = true;
            this.txtAsalSekolah.Size = new System.Drawing.Size(111, 23);
            this.txtAsalSekolah.TabIndex = 19;
            this.txtAsalSekolah.UseSelectable = true;
            this.txtAsalSekolah.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAsalSekolah.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(418, 213);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(121, 19);
            this.metroLabel9.TabIndex = 18;
            this.metroLabel9.Text = "Asal Sekolah         :";
            // 
            // cJurusan
            // 
            this.cJurusan.FormattingEnabled = true;
            this.cJurusan.ItemHeight = 23;
            this.cJurusan.Items.AddRange(new object[] {
            "Gizi",
            "Kesehatan Lingkungan",
            "Keperawatan",
            "Kebidanan"});
            this.cJurusan.Location = new System.Drawing.Point(590, 260);
            this.cJurusan.Name = "cJurusan";
            this.cJurusan.Size = new System.Drawing.Size(113, 29);
            this.cJurusan.TabIndex = 21;
            this.cJurusan.UseSelectable = true;
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(419, 260);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(119, 19);
            this.metroLabel10.TabIndex = 20;
            this.metroLabel10.Text = "Jurusan                :";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel11.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel11.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel11.ForeColor = System.Drawing.Color.Azure;
            this.metroLabel11.Location = new System.Drawing.Point(241, 60);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(287, 25);
            this.metroLabel11.Style = MetroFramework.MetroColorStyle.Black;
            this.metroLabel11.TabIndex = 23;
            this.metroLabel11.Text = "Pendaftaran Mahasiswa Baru D3";
            // 
            // btnSimpan
            // 
            this.btnSimpan.BackgroundImage = global::project2.Properties.Resources.Health_and_Wellness;
            this.btnSimpan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSimpan.Location = new System.Drawing.Point(638, 350);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(88, 74);
            this.btnSimpan.TabIndex = 22;
            this.btnSimpan.UseSelectable = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // Pendaftaran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 467);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.cJurusan);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.txtAsalSekolah);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.txtAlamatOrtu);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.txtNamaOrtu);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.cJK);
            this.Controls.Add(this.TTL);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.txtNoTelp);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.txtAlamat);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.txtAgama);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.txtNama);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.txtNISN);
            this.Controls.Add(this.label1);
            this.Name = "Pendaftaran";
            this.Text = "Pendaftaran";
            this.Load += new System.EventHandler(this.Pendaftaran_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel label1;
        private MetroFramework.Controls.MetroTextBox txtNISN;
        private MetroFramework.Controls.MetroTextBox txtNama;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtAgama;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtAlamat;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtNoTelp;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroDateTime TTL;
        private MetroFramework.Controls.MetroComboBox cJK;
        private MetroFramework.Controls.MetroTextBox txtNamaOrtu;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox txtAlamatOrtu;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox txtAsalSekolah;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroComboBox cJurusan;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroButton btnSimpan;
        private MetroFramework.Controls.MetroLabel metroLabel11;
    }
}