﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.IO;

namespace project2
{
    public partial class Pendaftaran : MetroForm
    {
        public Pendaftaran()
        {
            InitializeComponent();
        }
        public static string Nama, Jurusan, NISN;
        private void btnSimpan_Click(object sender, EventArgs e)
        {
            string Daftar;

            var Lines = File.ReadAllLines("DataPendaftarD3.txt");
            //if (Lines.Count() > 0)
            //{
            //    foreach (var cellArray in Lines)
            //    {
            //        var cell = cellArray.Split('#');
            //        if (cell.Equals(txtNISN.Text))
            //        {
            //            MessageBox.Show("NISN sudah Terdaftar masukan NISN lain!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            //        }

                    //else
                    //{
                        if ((txtNISN.Text.Length == 0) || (txtNama.Text.Length == 0) || (TTL.Text.Length == 0) || (cJK.Text.Length == 0) || (txtAgama.Text.Length == 0) || (txtAlamat.Text.Length == 0) || (txtNamaOrtu.Text.Length == 0) || (txtAlamatOrtu.Text.Length == 0) || (txtAsalSekolah.Text.Length == 0) || (cJurusan.Text.Length == 0))
                        {

                            MessageBox.Show("Silakan Selesaikan Pendaftaran", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            var aaa = MessageBox.Show("Apakah Yakin Data Diisi Dengan benar?", "information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (aaa.ToString() == "Yes")
                {
                    FileStream fs = new FileStream("DataPendaftarD3.txt", FileMode.Append, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);

                    Daftar = txtNISN.Text + "#" + txtNama.Text + "#" + TTL.Text + "#" + txtAlamat.Text + "#" + cJK.Text + "#" + txtAgama.Text + "#" + txtNoTelp.Text + "#" + txtNamaOrtu.Text + "#" + txtAlamatOrtu.Text + "#" + txtAsalSekolah.Text + "#" + cJurusan.Text + "#" + "Belum Bayar";
                    sw.WriteLine(Daftar);
                    sw.Flush();
                    sw.Close();
                    fs.Close();
                    MessageBox.Show("Berhasil Daftar", "information", MessageBoxButtons.OK, MessageBoxIcon.Information); //buat yang are you sure gtgt kalo messageboxbuttons                
                    Nama = txtNama.Text;
                    Jurusan = cJurusan.Text;
                    NISN = txtNISN.Text;

                    txtNISN.Clear();
                    //txtNama.Clear();

                    txtAgama.Clear();
                    txtAlamat.Clear();
                    txtNoTelp.Clear();
                    txtNamaOrtu.Clear();
                    txtAlamatOrtu.Clear();
                    txtAsalSekolah.Clear();

                    PembayaranPendaftaran obj = new PembayaranPendaftaran();
                    obj.Show();
                    this.Hide();



                }
            }
        }

        private void txtNISN_Leave(object sender, EventArgs e)
        {
            if (txtNISN.Text.Length == 10)
            {
                MessageBox.Show("NISN success");
            }
            else
            {
                MessageBox.Show("Masukan NISN dengan benar!");
            }
        }

        private void Pendaftaran_Load(object sender, EventArgs e)
        {

        }

        private void txtNoTelp_Leave(object sender, EventArgs e)
        {
            if (txtNoTelp.Text.Length <= 12)
            {
                MessageBox.Show("No Telp Valid");
            }
            else {
                MessageBox.Show("Masukan No Telp Dengan benar");
            }
        }
    }
}
